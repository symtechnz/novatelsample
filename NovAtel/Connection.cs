﻿using System;
using System.Buffers.Binary;
using System.IO;
using System.Threading.Tasks;

namespace NovAtel
{
    public class Connection
    {
        private const byte Sync0 = 0xAA;
        private const byte Sync1 = 0x44;
        private const byte Sync2 = 0x12;

        private readonly Stream _stream;

        public Connection(Stream stream)
        {
            _stream = stream;
        }

        public async Task ReadAsync(Action<Message> callback)
        {
            var buffer = new byte[1024];
            var length = 0;
            var offset = 0;

            while (true)
            {
                try
                {
                    if (offset > 0)
                    {
                        if (length > 0)
                            Array.Copy(buffer, offset, buffer, 0, length);
                        else
                            length = 0;

                        offset = 0;
                    }
                    else if (length == buffer.Length)
                    {
                        length = 0;
                    }

                    var end = offset + length;
                    var appended = await _stream.ReadAsync(buffer, end, buffer.Length - end).ConfigureAwait(false);
                    if (appended == -1)
                        return;

                    length += appended;

                    while (length > 0)
                    {
                        var consumed = TryReadMessage();
                        if (consumed == 0)
                            break;

                        offset += consumed;
                        length -= consumed;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            int TryReadMessage()
            {
                var memory = buffer.AsMemory(offset, length);
                var span = memory.Span;
                if (span[0] != Sync0)
                    return 1;

                if (span.Length < 28)
                    return 0;

                if (span[1] != Sync1)
                    return 2;

                if (span[2] != Sync2)
                    return 3;

                var headerLength = span[3];
                if (headerLength < 28)
                    return 3;

                if (span.Length < headerLength)
                    return 0;

                var messageLength = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(8, 2));
                var frameLength = headerLength + messageLength + 4;
                if (length < frameLength)
                    return 0;

                var message = new Message();
                message.MessageId = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(4, 2));
                message.MessageType = span[6];
                message.PortAddress = (Port)span[7];
                message.Sequence = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(10, 2));
                message.IdleTime = span[12];
                message.TimeStatus = span[13];
                message.Week = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(14, 2));
                message.Milliseconds = BinaryPrimitives.ReadUInt32LittleEndian(span.Slice(16, 4));
                message.ReceiverStatus = BinaryPrimitives.ReadUInt32LittleEndian(span.Slice(20, 4));
                message.Reserved = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(24, 2));
                message.ReceiverSoftwareVersion = BinaryPrimitives.ReadUInt16LittleEndian(span.Slice(26, 2));
                message.MessageContent = memory.Slice(headerLength, messageLength);
                var checksum = BinaryPrimitives.ReadUInt32LittleEndian(span.Slice(headerLength + messageLength, 4));
                var calculated = CalculateCrc(span.Slice(0, headerLength + messageLength));

                if (checksum == calculated)
                    callback(message);

                return frameLength;
            }
        }

        public async Task WriteAsync(Message message)
        {
            var buffer = new byte[28 + message.MessageLength + 4];
            buffer[0] = Sync0;
            buffer[1] = Sync1;
            buffer[2] = Sync2;
            buffer[3] = 28;
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(4, 2), message.MessageId);
            buffer[6] = (byte)message.MessageType;
            buffer[7] = (byte)message.PortAddress;
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(8, 2), message.MessageLength);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(10, 2), message.Sequence);
            buffer[12] = message.IdleTime;
            buffer[13] = message.TimeStatus;
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(14, 2), message.Week);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer.AsSpan(16, 4), message.Milliseconds);
            BinaryPrimitives.WriteUInt32LittleEndian(buffer.AsSpan(20, 4), message.ReceiverStatus);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(24, 2), message.Reserved);
            BinaryPrimitives.WriteUInt16LittleEndian(buffer.AsSpan(26, 2), message.ReceiverSoftwareVersion);
            message.MessageContent.Slice(0, message.MessageLength).CopyTo(buffer.AsMemory(28, message.MessageLength));
            var checksum = CalculateCrc(buffer.AsSpan(0, buffer.Length - 4));
            BinaryPrimitives.WriteUInt32LittleEndian(buffer.AsSpan(buffer.Length - 4, 4), checksum);
            await _stream.WriteAsync(buffer).ConfigureAwait(false);
        }

        private static uint CalculateCrc(ReadOnlySpan<byte> buffer)
        {
            const uint CRC32_POLYNOMIAL = 0xEDB88320;
            uint crc = 0;

            for (int i = 0; i < buffer.Length; i++)
            {
                uint temp = (crc ^ buffer[i]) & 0xff;

                for (int j = 0; j < 8; j++)
                {
                    if ((temp & 1) == 1)
                        temp = (temp >> 1) ^ CRC32_POLYNOMIAL;
                    else
                        temp >>= 1;
                }

                crc = (crc >> 8) ^ temp;
            }

            return crc;
        }
    }
}
