﻿namespace NovAtel
{
    public readonly struct MessageType
    {
        public static readonly MessageType Original = new MessageType(0);
        public static readonly MessageType Response = new MessageType(1);

        private readonly byte _value;

        public byte MeasurementSource => (byte)(_value & 0x1F);

        public MessageFormat Format => (MessageFormat)((_value >> 5) & 2);

        public bool IsOriginal => (_value & 0x80) == 0;

        public bool IsResponse => (_value & 0x80) != 0;

        public MessageType(byte value)
        {
            _value = value;
        }

        public static implicit operator MessageType(byte value) => new MessageType(value);

        public static explicit operator byte(MessageType messageTye) => messageTye._value;
    }
}
