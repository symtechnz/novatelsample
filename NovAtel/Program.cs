﻿using NovAtel.MessageHandlers;
using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NovAtel
{
    class Program
    {
        static Dictionary<int, IMessageHandler> _handlers = new Dictionary<int, IMessageHandler>();

        static async Task Main(string[] args)
        {
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes().Where(t => !t.IsInterface && !t.IsAbstract && typeof(IMessageHandler).IsAssignableFrom(t)))
            {
                var instance = (IMessageHandler)Activator.CreateInstance(type);
                _handlers.Add(instance.MessageId, instance);
            }

            var serial = new SerialPort(args.Length > 0 ? args[0] : "COM4");
            serial.BaudRate = 9600;
            serial.Open();

            var connection = new Connection(serial.BaseStream);
            var read = connection.ReadAsync(OnMessageReceived);

            await connection.WriteAsync(CreateLogMessage(Port.ThisPort0, 0x25, Trigger.Once));
            await connection.WriteAsync(CreateLogMessage(Port.ThisPort0, 0x2A, Trigger.OnTime, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1)));

            Console.ReadLine();

            await connection.WriteAsync(CreateUnlogMessage(Port.ThisPort0, 0x2A));

            Console.ReadLine();
        }

        static Message CreateLogMessage(Port port, ushort message, Trigger trigger, TimeSpan period = default, TimeSpan offset = default, bool hold = false)
        {
            var content = new byte[32];
            BinaryPrimitives.WriteUInt32LittleEndian(content.AsSpan(0, 4), (uint)port);
            BinaryPrimitives.WriteUInt16LittleEndian(content.AsSpan(4, 2), message);
            content[6] = 0x00; // Binary, request
            content[7] = 0x00; // Reserved
            BinaryPrimitives.WriteUInt32LittleEndian(content.AsSpan(8, 4), (uint)trigger);
            BinaryPrimitives.WriteDoubleLittleEndian(content.AsSpan(12, 8), period.TotalSeconds);
            BinaryPrimitives.WriteDoubleLittleEndian(content.AsSpan(20, 8), offset.TotalSeconds);
            BinaryPrimitives.WriteUInt32LittleEndian(content.AsSpan(28, 4), hold ? 1u : 0);

            return new Message
            {
                MessageId = 1,
                MessageType = MessageType.Original,
                PortAddress = Port.ThisPort0,
                MessageLength = 32,
                Sequence = 0,
                MessageContent = content,
            };
        }

        static Message CreateUnlogMessage(Port port, ushort message)
        {
            var content = new byte[8];
            BinaryPrimitives.WriteUInt32LittleEndian(content.AsSpan(0, 4), (uint)port);
            BinaryPrimitives.WriteUInt16LittleEndian(content.AsSpan(4, 2), message);
            content[6] = 0x00; // Binary, request
            content[7] = 0x00; // Reserved

            return new Message
            {
                MessageId = 36,
                MessageType = MessageType.Original,
                PortAddress = Port.ThisPort0,
                MessageLength = 8,
                Sequence = 0,
                MessageContent = content,
            };
        }

        static void OnMessageReceived(Message message)
        {
            if (message.MessageType.IsResponse)
            {
                var text = Encoding.ASCII.GetString(message.MessageContent.Span.Slice(4));
                if (_handlers.TryGetValue(message.MessageId, out var handler))
                    Console.WriteLine("Response to '{0}' received: {1}", handler.MessageName, text);
                else
                    Console.WriteLine("Response to message {0} received: {1}", message.MessageId, text);
            }
            else
            {
                if (_handlers.TryGetValue(message.MessageId, out var handler))
                {
                    try
                    {
                        handler.Handle(message);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An unhandled exception occurred while processing '{0}' message", handler.MessageName);
                        Console.Error.WriteLine(e.Message);
                    }
                }
                else
                    Console.WriteLine("Unknkown message '{0}' received", message.MessageId);
            }
        }
    }
}
