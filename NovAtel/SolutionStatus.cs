﻿using System.ComponentModel;

namespace NovAtel
{
    public enum SolutionStatus
    {
        [Description("Solution status not set")]
        NOT_SET = -2,
        MIN = -1,
        [Description("Solution computed")]
        SOLUTION_COMPUTED = 0,
        [Description("Insufficient observations")]
        INSUFFICIENT_OBS = 1,
        [Description("No convergence")]
        NO_CONVERGENCE = 2,
        [Description("Singular AtPA matrix")]
        SINGULAR_AtPA_MATRIX = 3,
        [Description("Covariance trace exceeds maximum (trace > 1000 m)")]
        BIG_COVARIANCE_TRACE = 4,
        [Description("Test distance exceeded (maximum of 3 rejections if distance > 10 km)")]
        BIG_TEST_DISTANCE = 5,
        [Description("Converging from cold start")]
        COLD_START = 6,
        [Description("CoCom limits exceeded")]
        SPEED_OR_HEIGHT_LIMITS_EXCEEDED = 7,
        [Description("Variance exceeds limits")]
        VARIANCE_EXCEEDS_LIMIT = 8,
        [Description("Residuals are too large")]
        RESIDUALS_ARE_TOO_LARGE = 9,
        [Description("Delta position is too large")]
        DELTA_POSITION_IS_TOO_LARGE = 10,
        [Description("Negative variance")]
        NEGATIVE_VARIANCE = 11,
        [Description("The position is old")]
        OLD_SOLUTION = 12,
        [Description("Integrity warning")]
        INTEGRITY_WARNING = 13,
        [Description("INS has not started yet")]
        INS_INACTIVE = 14,
        [Description("INS doing its coarse alignment")]
        INS_ALIGNING = 15,
        [Description("INS position is bad")]
        INS_BAD = 16,
        [Description("No IMU detected")]
        IMU_UNPLUGGED = 17,
        [Description("Not enough satellites to verify FIX POSITION")]
        PENDING = 18,
        [Description("Fixed position is not valid")]
        INVALID_FIX = 19,
        [Description("Position type (HP or XP) not authorized")]
        UNAUTHORIZED = 20,
        [Description("Selected RTK antenna mode not possible")]
        ANTENNA_WARNING = 21,
        [Description("Logging rate not supported for this solution type")]
        INVALID_RATE = 22,
        MAX
    }
}
