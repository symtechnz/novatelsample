﻿using System;
using System.Text;

namespace NovAtel
{
    static class SpanExtensions
    {
        public static string AsAsciiString(this ReadOnlySpan<byte> span)
        {
            for (int i = 0; i < span.Length; i++)
                if (span[i] == 0)
                    return Encoding.ASCII.GetString(span.Slice(0, i));

            return Encoding.ASCII.GetString(span);
        }
    }
}
