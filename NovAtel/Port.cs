﻿using System.ComponentModel;

namespace NovAtel
{
    public enum Port
    {
        [Description("NO_PORTS")]
        NoPorts = 0,
        [Description("COM1_ALL")]
        Com1All = 1,
        [Description("COM2_ALL")]
        Com2All = 2,
        [Description("COM3_ALL")]
        Com3All = 3,
        [Description("THISPORT_ALL")]
        ThisPortAll = 6,
        [Description("FILE_ALL")]
        FileAll = 7,
        [Description("ALL_PORTS")]
        AllPorts = 8,
        [Description("USB1_ALL")]
        Usb1All = 13,
        [Description("USB2_ALL")]
        Usb2All = 14,
        [Description("USB3_ALL")]
        Usb3All = 15,
        [Description("AUX_ALL")]
        AuxAll = 16,
        [Description("COM4_ALL")]
        Com4All = 19,
        [Description("ETH1_ALL")]
        Eth1All = 20,
        [Description("IMU_ALL")]
        ImuAll = 21,
        [Description("ICOM1_ALL")]
        ICom1All = 23,
        [Description("ICOM2_ALL")]
        ICom2All = 24,
        [Description("ICOM3_ALL")]
        ICom3All = 25,
        [Description("NCOM1_ALL")]
        NCom1All = 26,
        [Description("NCOM2_ALL")]
        NCom2All = 27,
        [Description("NCOM3_ALL")]
        NCom3All = 28,
        [Description("ICOM4_ALL")]
        ICom4All = 29,
        [Description("WCOM1_ALL")]
        WCom1All = 30,
        [Description("COM1")]
        Com1Port0 = 32,
        [Description("COM1_1")]
        Com1Port1 = Com1Port0 + 1,
        [Description("COM1_2")]
        Com1Port2 = Com1Port0 + 2,
        [Description("COM1_3")]
        Com1Port3 = Com1Port0 + 3,
        [Description("COM1_4")]
        Com1Port4 = Com1Port0 + 4,
        [Description("COM1_5")]
        Com1Port5 = Com1Port0 + 5,
        [Description("COM1_6")]
        Com1Port6 = Com1Port0 + 6,
        [Description("COM1_7")]
        Com1Port7 = Com1Port0 + 7,
        [Description("COM1_8")]
        Com1Port8 = Com1Port0 + 8,
        [Description("COM1_9")]
        Com1Port9 = Com1Port0 + 9,
        [Description("COM1_10")]
        Com1Port10 = Com1Port0 + 10,
        [Description("COM1_11")]
        Com1Port11 = Com1Port0 + 11,
        [Description("COM1_12")]
        Com1Port12 = Com1Port0 + 12,
        [Description("COM1_13")]
        Com1Port13 = Com1Port0 + 13,
        [Description("COM1_14")]
        Com1Port14 = Com1Port0 + 14,
        [Description("COM1_15")]
        Com1Port15 = Com1Port0 + 15,
        [Description("COM1_16")]
        Com1Port16 = Com1Port0 + 16,
        [Description("COM1_17")]
        Com1Port17 = Com1Port0 + 17,
        [Description("COM1_18")]
        Com1Port18 = Com1Port0 + 18,
        [Description("COM1_19")]
        Com1Port19 = Com1Port0 + 19,
        [Description("COM1_20")]
        Com1Port20 = Com1Port0 + 20,
        [Description("COM1_21")]
        Com1Port21 = Com1Port0 + 21,
        [Description("COM1_22")]
        Com1Port22 = Com1Port0 + 22,
        [Description("COM1_23")]
        Com1Port23 = Com1Port0 + 23,
        [Description("COM1_24")]
        Com1Port24 = Com1Port0 + 24,
        [Description("COM1_25")]
        Com1Port25 = Com1Port0 + 25,
        [Description("COM1_26")]
        Com1Port26 = Com1Port0 + 26,
        [Description("COM1_27")]
        Com1Port27 = Com1Port0 + 27,
        [Description("COM1_28")]
        Com1Port28 = Com1Port0 + 28,
        [Description("COM1_29")]
        Com1Port29 = Com1Port0 + 29,
        [Description("COM1_30")]
        Com1Port30 = Com1Port0 + 30,
        [Description("COM1_31")]
        Com1Port31 = Com1Port0 + 31,
        [Description("COM2")]
        Com2Port0 = 64,
        [Description("COM2_1")]
        Com2Port1 = Com2Port0 + 1,
        [Description("COM2_2")]
        Com2Port2 = Com2Port0 + 2,
        [Description("COM2_3")]
        Com2Port3 = Com2Port0 + 3,
        [Description("COM2_4")]
        Com2Port4 = Com2Port0 + 4,
        [Description("COM2_5")]
        Com2Port5 = Com2Port0 + 5,
        [Description("COM2_6")]
        Com2Port6 = Com2Port0 + 6,
        [Description("COM2_7")]
        Com2Port7 = Com2Port0 + 7,
        [Description("COM2_8")]
        Com2Port8 = Com2Port0 + 8,
        [Description("COM2_9")]
        Com2Port9 = Com2Port0 + 9,
        [Description("COM2_10")]
        Com2Port10 = Com2Port0 + 10,
        [Description("COM2_11")]
        Com2Port11 = Com2Port0 + 11,
        [Description("COM2_12")]
        Com2Port12 = Com2Port0 + 12,
        [Description("COM2_13")]
        Com2Port13 = Com2Port0 + 13,
        [Description("COM2_14")]
        Com2Port14 = Com2Port0 + 14,
        [Description("COM2_15")]
        Com2Port15 = Com2Port0 + 15,
        [Description("COM2_16")]
        Com2Port16 = Com2Port0 + 16,
        [Description("COM2_17")]
        Com2Port17 = Com2Port0 + 17,
        [Description("COM2_18")]
        Com2Port18 = Com2Port0 + 18,
        [Description("COM2_19")]
        Com2Port19 = Com2Port0 + 19,
        [Description("COM2_20")]
        Com2Port20 = Com2Port0 + 20,
        [Description("COM2_21")]
        Com2Port21 = Com2Port0 + 21,
        [Description("COM2_22")]
        Com2Port22 = Com2Port0 + 22,
        [Description("COM2_23")]
        Com2Port23 = Com2Port0 + 23,
        [Description("COM2_24")]
        Com2Port24 = Com2Port0 + 24,
        [Description("COM2_25")]
        Com2Port25 = Com2Port0 + 25,
        [Description("COM2_26")]
        Com2Port26 = Com2Port0 + 26,
        [Description("COM2_27")]
        Com2Port27 = Com2Port0 + 27,
        [Description("COM2_28")]
        Com2Port28 = Com2Port0 + 28,
        [Description("COM2_29")]
        Com2Port29 = Com2Port0 + 29,
        [Description("COM2_30")]
        Com2Port30 = Com2Port0 + 30,
        [Description("COM2_31")]
        Com2Port31 = Com2Port0 + 31,
        [Description("COM3")]
        Com3Port0 = 96,
        [Description("COM3_1")]
        Com3Port1 = Com3Port0 + 1,
        [Description("COM3_2")]
        Com3Port2 = Com3Port0 + 2,
        [Description("COM3_3")]
        Com3Port3 = Com3Port0 + 3,
        [Description("COM3_4")]
        Com3Port4 = Com3Port0 + 4,
        [Description("COM3_5")]
        Com3Port5 = Com3Port0 + 5,
        [Description("COM3_6")]
        Com3Port6 = Com3Port0 + 6,
        [Description("COM3_7")]
        Com3Port7 = Com3Port0 + 7,
        [Description("COM3_8")]
        Com3Port8 = Com3Port0 + 8,
        [Description("COM3_9")]
        Com3Port9 = Com3Port0 + 9,
        [Description("COM3_10")]
        Com3Port10 = Com3Port0 + 10,
        [Description("COM3_11")]
        Com3Port11 = Com3Port0 + 11,
        [Description("COM3_12")]
        Com3Port12 = Com3Port0 + 12,
        [Description("COM3_13")]
        Com3Port13 = Com3Port0 + 13,
        [Description("COM3_14")]
        Com3Port14 = Com3Port0 + 14,
        [Description("COM3_15")]
        Com3Port15 = Com3Port0 + 15,
        [Description("COM3_16")]
        Com3Port16 = Com3Port0 + 16,
        [Description("COM3_17")]
        Com3Port17 = Com3Port0 + 17,
        [Description("COM3_18")]
        Com3Port18 = Com3Port0 + 18,
        [Description("COM3_19")]
        Com3Port19 = Com3Port0 + 19,
        [Description("COM3_20")]
        Com3Port20 = Com3Port0 + 20,
        [Description("COM3_21")]
        Com3Port21 = Com3Port0 + 21,
        [Description("COM3_22")]
        Com3Port22 = Com3Port0 + 22,
        [Description("COM3_23")]
        Com3Port23 = Com3Port0 + 23,
        [Description("COM3_24")]
        Com3Port24 = Com3Port0 + 24,
        [Description("COM3_25")]
        Com3Port25 = Com3Port0 + 25,
        [Description("COM3_26")]
        Com3Port26 = Com3Port0 + 26,
        [Description("COM3_27")]
        Com3Port27 = Com3Port0 + 27,
        [Description("COM3_28")]
        Com3Port28 = Com3Port0 + 28,
        [Description("COM3_29")]
        Com3Port29 = Com3Port0 + 29,
        [Description("COM3_30")]
        Com3Port30 = Com3Port0 + 30,
        [Description("COM3_31")]
        Com3Port31 = Com3Port0 + 31,
        [Description("SPECIAL")]
        SpecialPort0 = 160,
        [Description("SPECIAL_1")]
        SpecialPort1 = SpecialPort0 + 1,
        [Description("SPECIAL_2")]
        SpecialPort2 = SpecialPort0 + 2,
        [Description("SPECIAL_3")]
        SpecialPort3 = SpecialPort0 + 3,
        [Description("SPECIAL_4")]
        SpecialPort4 = SpecialPort0 + 4,
        [Description("SPECIAL_5")]
        SpecialPort5 = SpecialPort0 + 5,
        [Description("SPECIAL_6")]
        SpecialPort6 = SpecialPort0 + 6,
        [Description("SPECIAL_7")]
        SpecialPort7 = SpecialPort0 + 7,
        [Description("SPECIAL_8")]
        SpecialPort8 = SpecialPort0 + 8,
        [Description("SPECIAL_9")]
        SpecialPort9 = SpecialPort0 + 9,
        [Description("SPECIAL_10")]
        SpecialPort10 = SpecialPort0 + 10,
        [Description("SPECIAL_11")]
        SpecialPort11 = SpecialPort0 + 11,
        [Description("SPECIAL_12")]
        SpecialPort12 = SpecialPort0 + 12,
        [Description("SPECIAL_13")]
        SpecialPort13 = SpecialPort0 + 13,
        [Description("SPECIAL_14")]
        SpecialPort14 = SpecialPort0 + 14,
        [Description("SPECIAL_15")]
        SpecialPort15 = SpecialPort0 + 15,
        [Description("SPECIAL_16")]
        SpecialPort16 = SpecialPort0 + 16,
        [Description("SPECIAL_17")]
        SpecialPort17 = SpecialPort0 + 17,
        [Description("SPECIAL_18")]
        SpecialPort18 = SpecialPort0 + 18,
        [Description("SPECIAL_19")]
        SpecialPort19 = SpecialPort0 + 19,
        [Description("SPECIAL_20")]
        SpecialPort20 = SpecialPort0 + 20,
        [Description("SPECIAL_21")]
        SpecialPort21 = SpecialPort0 + 21,
        [Description("SPECIAL_22")]
        SpecialPort22 = SpecialPort0 + 22,
        [Description("SPECIAL_23")]
        SpecialPort23 = SpecialPort0 + 23,
        [Description("SPECIAL_24")]
        SpecialPort24 = SpecialPort0 + 24,
        [Description("SPECIAL_25")]
        SpecialPort25 = SpecialPort0 + 25,
        [Description("SPECIAL_26")]
        SpecialPort26 = SpecialPort0 + 26,
        [Description("SPECIAL_27")]
        SpecialPort27 = SpecialPort0 + 27,
        [Description("SPECIAL_28")]
        SpecialPort28 = SpecialPort0 + 28,
        [Description("SPECIAL_29")]
        SpecialPort29 = SpecialPort0 + 29,
        [Description("SPECIAL_30")]
        SpecialPort30 = SpecialPort0 + 30,
        [Description("SPECIAL_31")]
        SpecialPort31 = SpecialPort0 + 31,
        [Description("THISPORT")]
        ThisPort0 = 192,
        [Description("THISPORT_1")]
        ThisPort1 = ThisPort0 + 1,
        [Description("THISPORT_2")]
        ThisPort2 = ThisPort0 + 2,
        [Description("THISPORT_3")]
        ThisPort3 = ThisPort0 + 3,
        [Description("THISPORT_4")]
        ThisPort4 = ThisPort0 + 4,
        [Description("THISPORT_5")]
        ThisPort5 = ThisPort0 + 5,
        [Description("THISPORT_6")]
        ThisPort6 = ThisPort0 + 6,
        [Description("THISPORT_7")]
        ThisPort7 = ThisPort0 + 7,
        [Description("THISPORT_8")]
        ThisPort8 = ThisPort0 + 8,
        [Description("THISPORT_9")]
        ThisPort9 = ThisPort0 + 9,
        [Description("THISPORT_10")]
        ThisPort10 = ThisPort0 + 10,
        [Description("THISPORT_11")]
        ThisPort11 = ThisPort0 + 11,
        [Description("THISPORT_12")]
        ThisPort12 = ThisPort0 + 12,
        [Description("THISPORT_13")]
        ThisPort13 = ThisPort0 + 13,
        [Description("THISPORT_14")]
        ThisPort14 = ThisPort0 + 14,
        [Description("THISPORT_15")]
        ThisPort15 = ThisPort0 + 15,
        [Description("THISPORT_16")]
        ThisPort16 = ThisPort0 + 16,
        [Description("THISPORT_17")]
        ThisPort17 = ThisPort0 + 17,
        [Description("THISPORT_18")]
        ThisPort18 = ThisPort0 + 18,
        [Description("THISPORT_19")]
        ThisPort19 = ThisPort0 + 19,
        [Description("THISPORT_20")]
        ThisPort20 = ThisPort0 + 20,
        [Description("THISPORT_21")]
        ThisPort21 = ThisPort0 + 21,
        [Description("THISPORT_22")]
        ThisPort22 = ThisPort0 + 22,
        [Description("THISPORT_23")]
        ThisPort23 = ThisPort0 + 23,
        [Description("THISPORT_24")]
        ThisPort24 = ThisPort0 + 24,
        [Description("THISPORT_25")]
        ThisPort25 = ThisPort0 + 25,
        [Description("THISPORT_26")]
        ThisPort26 = ThisPort0 + 26,
        [Description("THISPORT_27")]
        ThisPort27 = ThisPort0 + 27,
        [Description("THISPORT_28")]
        ThisPort28 = ThisPort0 + 28,
        [Description("THISPORT_29")]
        ThisPort29 = ThisPort0 + 29,
        [Description("THISPORT_30")]
        ThisPort30 = ThisPort0 + 30,
        [Description("THISPORT_31")]
        ThisPort31 = ThisPort0 + 31,
        [Description("FILE")]
        FilePort0 = 224,
        [Description("FILE_1")]
        FilePort1 = FilePort0 + 1,
        [Description("FILE_2")]
        FilePort2 = FilePort0 + 2,
        [Description("FILE_3")]
        FilePort3 = FilePort0 + 3,
        [Description("FILE_4")]
        FilePort4 = FilePort0 + 4,
        [Description("FILE_5")]
        FilePort5 = FilePort0 + 5,
        [Description("FILE_6")]
        FilePort6 = FilePort0 + 6,
        [Description("FILE_7")]
        FilePort7 = FilePort0 + 7,
        [Description("FILE_8")]
        FilePort8 = FilePort0 + 8,
        [Description("FILE_9")]
        FilePort9 = FilePort0 + 9,
        [Description("FILE_10")]
        FilePort10 = FilePort0 + 10,
        [Description("FILE_11")]
        FilePort11 = FilePort0 + 11,
        [Description("FILE_12")]
        FilePort12 = FilePort0 + 12,
        [Description("FILE_13")]
        FilePort13 = FilePort0 + 13,
        [Description("FILE_14")]
        FilePort14 = FilePort0 + 14,
        [Description("FILE_15")]
        FilePort15 = FilePort0 + 15,
        [Description("FILE_16")]
        FilePort16 = FilePort0 + 16,
        [Description("FILE_17")]
        FilePort17 = FilePort0 + 17,
        [Description("FILE_18")]
        FilePort18 = FilePort0 + 18,
        [Description("FILE_19")]
        FilePort19 = FilePort0 + 19,
        [Description("FILE_20")]
        FilePort20 = FilePort0 + 20,
        [Description("FILE_21")]
        FilePort21 = FilePort0 + 21,
        [Description("FILE_22")]
        FilePort22 = FilePort0 + 22,
        [Description("FILE_23")]
        FilePort23 = FilePort0 + 23,
        [Description("FILE_24")]
        FilePort24 = FilePort0 + 24,
        [Description("FILE_25")]
        FilePort25 = FilePort0 + 25,
        [Description("FILE_26")]
        FilePort26 = FilePort0 + 26,
        [Description("FILE_27")]
        FilePort27 = FilePort0 + 27,
        [Description("FILE_28")]
        FilePort28 = FilePort0 + 28,
        [Description("FILE_29")]
        FilePort29 = FilePort0 + 29,
        [Description("FILE_30")]
        FilePort30 = FilePort0 + 30,
        [Description("FILE_31")]
        FilePort31 = FilePort0 + 31,
        [Description("USB1")]
        Usb1Port0 = 1440,
        [Description("USB1_1")]
        Usb1Port1 = Usb1Port0 + 1,
        [Description("USB1_2")]
        Usb1Port2 = Usb1Port0 + 2,
        [Description("USB1_3")]
        Usb1Port3 = Usb1Port0 + 3,
        [Description("USB1_4")]
        Usb1Port4 = Usb1Port0 + 4,
        [Description("USB1_5")]
        Usb1Port5 = Usb1Port0 + 5,
        [Description("USB1_6")]
        Usb1Port6 = Usb1Port0 + 6,
        [Description("USB1_7")]
        Usb1Port7 = Usb1Port0 + 7,
        [Description("USB1_8")]
        Usb1Port8 = Usb1Port0 + 8,
        [Description("USB1_9")]
        Usb1Port9 = Usb1Port0 + 9,
        [Description("USB1_10")]
        Usb1Port10 = Usb1Port0 + 10,
        [Description("USB1_11")]
        Usb1Port11 = Usb1Port0 + 11,
        [Description("USB1_12")]
        Usb1Port12 = Usb1Port0 + 12,
        [Description("USB1_13")]
        Usb1Port13 = Usb1Port0 + 13,
        [Description("USB1_14")]
        Usb1Port14 = Usb1Port0 + 14,
        [Description("USB1_15")]
        Usb1Port15 = Usb1Port0 + 15,
        [Description("USB1_16")]
        Usb1Port16 = Usb1Port0 + 16,
        [Description("USB1_17")]
        Usb1Port17 = Usb1Port0 + 17,
        [Description("USB1_18")]
        Usb1Port18 = Usb1Port0 + 18,
        [Description("USB1_19")]
        Usb1Port19 = Usb1Port0 + 19,
        [Description("USB1_20")]
        Usb1Port20 = Usb1Port0 + 20,
        [Description("USB1_21")]
        Usb1Port21 = Usb1Port0 + 21,
        [Description("USB1_22")]
        Usb1Port22 = Usb1Port0 + 22,
        [Description("USB1_23")]
        Usb1Port23 = Usb1Port0 + 23,
        [Description("USB1_24")]
        Usb1Port24 = Usb1Port0 + 24,
        [Description("USB1_25")]
        Usb1Port25 = Usb1Port0 + 25,
        [Description("USB1_26")]
        Usb1Port26 = Usb1Port0 + 26,
        [Description("USB1_27")]
        Usb1Port27 = Usb1Port0 + 27,
        [Description("USB1_28")]
        Usb1Port28 = Usb1Port0 + 28,
        [Description("USB1_29")]
        Usb1Port29 = Usb1Port0 + 29,
        [Description("USB1_30")]
        Usb1Port30 = Usb1Port0 + 30,
        [Description("USB1_31")]
        Usb1Port31 = Usb1Port0 + 31,
        [Description("USB2")]
        Usb2Port0 = 1696,
        [Description("USB2_1")]
        Usb2Port1 = Usb2Port0 + 1,
        [Description("USB2_2")]
        Usb2Port2 = Usb2Port0 + 2,
        [Description("USB2_3")]
        Usb2Port3 = Usb2Port0 + 3,
        [Description("USB2_4")]
        Usb2Port4 = Usb2Port0 + 4,
        [Description("USB2_5")]
        Usb2Port5 = Usb2Port0 + 5,
        [Description("USB2_6")]
        Usb2Port6 = Usb2Port0 + 6,
        [Description("USB2_7")]
        Usb2Port7 = Usb2Port0 + 7,
        [Description("USB2_8")]
        Usb2Port8 = Usb2Port0 + 8,
        [Description("USB2_9")]
        Usb2Port9 = Usb2Port0 + 9,
        [Description("USB2_10")]
        Usb2Port10 = Usb2Port0 + 10,
        [Description("USB2_11")]
        Usb2Port11 = Usb2Port0 + 11,
        [Description("USB2_12")]
        Usb2Port12 = Usb2Port0 + 12,
        [Description("USB2_13")]
        Usb2Port13 = Usb2Port0 + 13,
        [Description("USB2_14")]
        Usb2Port14 = Usb2Port0 + 14,
        [Description("USB2_15")]
        Usb2Port15 = Usb2Port0 + 15,
        [Description("USB2_16")]
        Usb2Port16 = Usb2Port0 + 16,
        [Description("USB2_17")]
        Usb2Port17 = Usb2Port0 + 17,
        [Description("USB2_18")]
        Usb2Port18 = Usb2Port0 + 18,
        [Description("USB2_19")]
        Usb2Port19 = Usb2Port0 + 19,
        [Description("USB2_20")]
        Usb2Port20 = Usb2Port0 + 20,
        [Description("USB2_21")]
        Usb2Port21 = Usb2Port0 + 21,
        [Description("USB2_22")]
        Usb2Port22 = Usb2Port0 + 22,
        [Description("USB2_23")]
        Usb2Port23 = Usb2Port0 + 23,
        [Description("USB2_24")]
        Usb2Port24 = Usb2Port0 + 24,
        [Description("USB2_25")]
        Usb2Port25 = Usb2Port0 + 25,
        [Description("USB2_26")]
        Usb2Port26 = Usb2Port0 + 26,
        [Description("USB2_27")]
        Usb2Port27 = Usb2Port0 + 27,
        [Description("USB2_28")]
        Usb2Port28 = Usb2Port0 + 28,
        [Description("USB2_29")]
        Usb2Port29 = Usb2Port0 + 29,
        [Description("USB2_30")]
        Usb2Port30 = Usb2Port0 + 30,
        [Description("USB2_31")]
        Usb2Port31 = Usb2Port0 + 31,
        [Description("USB3")]
        Usb3Port0 = 1952,
        [Description("USB3_1")]
        Usb3Port1 = Usb3Port0 + 1,
        [Description("USB3_2")]
        Usb3Port2 = Usb3Port0 + 2,
        [Description("USB3_3")]
        Usb3Port3 = Usb3Port0 + 3,
        [Description("USB3_4")]
        Usb3Port4 = Usb3Port0 + 4,
        [Description("USB3_5")]
        Usb3Port5 = Usb3Port0 + 5,
        [Description("USB3_6")]
        Usb3Port6 = Usb3Port0 + 6,
        [Description("USB3_7")]
        Usb3Port7 = Usb3Port0 + 7,
        [Description("USB3_8")]
        Usb3Port8 = Usb3Port0 + 8,
        [Description("USB3_9")]
        Usb3Port9 = Usb3Port0 + 9,
        [Description("USB3_10")]
        Usb3Port10 = Usb3Port0 + 10,
        [Description("USB3_11")]
        Usb3Port11 = Usb3Port0 + 11,
        [Description("USB3_12")]
        Usb3Port12 = Usb3Port0 + 12,
        [Description("USB3_13")]
        Usb3Port13 = Usb3Port0 + 13,
        [Description("USB3_14")]
        Usb3Port14 = Usb3Port0 + 14,
        [Description("USB3_15")]
        Usb3Port15 = Usb3Port0 + 15,
        [Description("USB3_16")]
        Usb3Port16 = Usb3Port0 + 16,
        [Description("USB3_17")]
        Usb3Port17 = Usb3Port0 + 17,
        [Description("USB3_18")]
        Usb3Port18 = Usb3Port0 + 18,
        [Description("USB3_19")]
        Usb3Port19 = Usb3Port0 + 19,
        [Description("USB3_20")]
        Usb3Port20 = Usb3Port0 + 20,
        [Description("USB3_21")]
        Usb3Port21 = Usb3Port0 + 21,
        [Description("USB3_22")]
        Usb3Port22 = Usb3Port0 + 22,
        [Description("USB3_23")]
        Usb3Port23 = Usb3Port0 + 23,
        [Description("USB3_24")]
        Usb3Port24 = Usb3Port0 + 24,
        [Description("USB3_25")]
        Usb3Port25 = Usb3Port0 + 25,
        [Description("USB3_26")]
        Usb3Port26 = Usb3Port0 + 26,
        [Description("USB3_27")]
        Usb3Port27 = Usb3Port0 + 27,
        [Description("USB3_28")]
        Usb3Port28 = Usb3Port0 + 28,
        [Description("USB3_29")]
        Usb3Port29 = Usb3Port0 + 29,
        [Description("USB3_30")]
        Usb3Port30 = Usb3Port0 + 30,
        [Description("USB3_31")]
        Usb3Port31 = Usb3Port0 + 31,
        [Description("AUX")]
        AuxPort0 = 2208,
        [Description("AUX_1")]
        AuxPort1 = AuxPort0 + 1,
        [Description("AUX_2")]
        AuxPort2 = AuxPort0 + 2,
        [Description("AUX_3")]
        AuxPort3 = AuxPort0 + 3,
        [Description("AUX_4")]
        AuxPort4 = AuxPort0 + 4,
        [Description("AUX_5")]
        AuxPort5 = AuxPort0 + 5,
        [Description("AUX_6")]
        AuxPort6 = AuxPort0 + 6,
        [Description("AUX_7")]
        AuxPort7 = AuxPort0 + 7,
        [Description("AUX_8")]
        AuxPort8 = AuxPort0 + 8,
        [Description("AUX_9")]
        AuxPort9 = AuxPort0 + 9,
        [Description("AUX_10")]
        AuxPort10 = AuxPort0 + 10,
        [Description("AUX_11")]
        AuxPort11 = AuxPort0 + 11,
        [Description("AUX_12")]
        AuxPort12 = AuxPort0 + 12,
        [Description("AUX_13")]
        AuxPort13 = AuxPort0 + 13,
        [Description("AUX_14")]
        AuxPort14 = AuxPort0 + 14,
        [Description("AUX_15")]
        AuxPort15 = AuxPort0 + 15,
        [Description("AUX_16")]
        AuxPort16 = AuxPort0 + 16,
        [Description("AUX_17")]
        AuxPort17 = AuxPort0 + 17,
        [Description("AUX_18")]
        AuxPort18 = AuxPort0 + 18,
        [Description("AUX_19")]
        AuxPort19 = AuxPort0 + 19,
        [Description("AUX_20")]
        AuxPort20 = AuxPort0 + 20,
        [Description("AUX_21")]
        AuxPort21 = AuxPort0 + 21,
        [Description("AUX_22")]
        AuxPort22 = AuxPort0 + 22,
        [Description("AUX_23")]
        AuxPort23 = AuxPort0 + 23,
        [Description("AUX_24")]
        AuxPort24 = AuxPort0 + 24,
        [Description("AUX_25")]
        AuxPort25 = AuxPort0 + 25,
        [Description("AUX_26")]
        AuxPort26 = AuxPort0 + 26,
        [Description("AUX_27")]
        AuxPort27 = AuxPort0 + 27,
        [Description("AUX_28")]
        AuxPort28 = AuxPort0 + 28,
        [Description("AUX_29")]
        AuxPort29 = AuxPort0 + 29,
        [Description("AUX_30")]
        AuxPort30 = AuxPort0 + 30,
        [Description("AUX_31")]
        AuxPort31 = AuxPort0 + 31,
        [Description("COM4")]
        Com4Port0 = 2976,
        [Description("COM4_1")]
        Com4Port1 = Com4Port0 + 1,
        [Description("COM4_2")]
        Com4Port2 = Com4Port0 + 2,
        [Description("COM4_3")]
        Com4Port3 = Com4Port0 + 3,
        [Description("COM4_4")]
        Com4Port4 = Com4Port0 + 4,
        [Description("COM4_5")]
        Com4Port5 = Com4Port0 + 5,
        [Description("COM4_6")]
        Com4Port6 = Com4Port0 + 6,
        [Description("COM4_7")]
        Com4Port7 = Com4Port0 + 7,
        [Description("COM4_8")]
        Com4Port8 = Com4Port0 + 8,
        [Description("COM4_9")]
        Com4Port9 = Com4Port0 + 9,
        [Description("COM4_10")]
        Com4Port10 = Com4Port0 + 10,
        [Description("COM4_11")]
        Com4Port11 = Com4Port0 + 11,
        [Description("COM4_12")]
        Com4Port12 = Com4Port0 + 12,
        [Description("COM4_13")]
        Com4Port13 = Com4Port0 + 13,
        [Description("COM4_14")]
        Com4Port14 = Com4Port0 + 14,
        [Description("COM4_15")]
        Com4Port15 = Com4Port0 + 15,
        [Description("COM4_16")]
        Com4Port16 = Com4Port0 + 16,
        [Description("COM4_17")]
        Com4Port17 = Com4Port0 + 17,
        [Description("COM4_18")]
        Com4Port18 = Com4Port0 + 18,
        [Description("COM4_19")]
        Com4Port19 = Com4Port0 + 19,
        [Description("COM4_20")]
        Com4Port20 = Com4Port0 + 20,
        [Description("COM4_21")]
        Com4Port21 = Com4Port0 + 21,
        [Description("COM4_22")]
        Com4Port22 = Com4Port0 + 22,
        [Description("COM4_23")]
        Com4Port23 = Com4Port0 + 23,
        [Description("COM4_24")]
        Com4Port24 = Com4Port0 + 24,
        [Description("COM4_25")]
        Com4Port25 = Com4Port0 + 25,
        [Description("COM4_26")]
        Com4Port26 = Com4Port0 + 26,
        [Description("COM4_27")]
        Com4Port27 = Com4Port0 + 27,
        [Description("COM4_28")]
        Com4Port28 = Com4Port0 + 28,
        [Description("COM4_29")]
        Com4Port29 = Com4Port0 + 29,
        [Description("COM4_30")]
        Com4Port30 = Com4Port0 + 30,
        [Description("COM4_31")]
        Com4Port31 = Com4Port0 + 31,
        [Description("ETH1")]
        Eth1Port0 = 3232,
        [Description("ETH1_1")]
        Eth1Port1 = Eth1Port0 + 1,
        [Description("ETH1_2")]
        Eth1Port2 = Eth1Port0 + 2,
        [Description("ETH1_3")]
        Eth1Port3 = Eth1Port0 + 3,
        [Description("ETH1_4")]
        Eth1Port4 = Eth1Port0 + 4,
        [Description("ETH1_5")]
        Eth1Port5 = Eth1Port0 + 5,
        [Description("ETH1_6")]
        Eth1Port6 = Eth1Port0 + 6,
        [Description("ETH1_7")]
        Eth1Port7 = Eth1Port0 + 7,
        [Description("ETH1_8")]
        Eth1Port8 = Eth1Port0 + 8,
        [Description("ETH1_9")]
        Eth1Port9 = Eth1Port0 + 9,
        [Description("ETH1_10")]
        Eth1Port10 = Eth1Port0 + 10,
        [Description("ETH1_11")]
        Eth1Port11 = Eth1Port0 + 11,
        [Description("ETH1_12")]
        Eth1Port12 = Eth1Port0 + 12,
        [Description("ETH1_13")]
        Eth1Port13 = Eth1Port0 + 13,
        [Description("ETH1_14")]
        Eth1Port14 = Eth1Port0 + 14,
        [Description("ETH1_15")]
        Eth1Port15 = Eth1Port0 + 15,
        [Description("ETH1_16")]
        Eth1Port16 = Eth1Port0 + 16,
        [Description("ETH1_17")]
        Eth1Port17 = Eth1Port0 + 17,
        [Description("ETH1_18")]
        Eth1Port18 = Eth1Port0 + 18,
        [Description("ETH1_19")]
        Eth1Port19 = Eth1Port0 + 19,
        [Description("ETH1_20")]
        Eth1Port20 = Eth1Port0 + 20,
        [Description("ETH1_21")]
        Eth1Port21 = Eth1Port0 + 21,
        [Description("ETH1_22")]
        Eth1Port22 = Eth1Port0 + 22,
        [Description("ETH1_23")]
        Eth1Port23 = Eth1Port0 + 23,
        [Description("ETH1_24")]
        Eth1Port24 = Eth1Port0 + 24,
        [Description("ETH1_25")]
        Eth1Port25 = Eth1Port0 + 25,
        [Description("ETH1_26")]
        Eth1Port26 = Eth1Port0 + 26,
        [Description("ETH1_27")]
        Eth1Port27 = Eth1Port0 + 27,
        [Description("ETH1_28")]
        Eth1Port28 = Eth1Port0 + 28,
        [Description("ETH1_29")]
        Eth1Port29 = Eth1Port0 + 29,
        [Description("ETH1_30")]
        Eth1Port30 = Eth1Port0 + 30,
        [Description("ETH1_31")]
        Eth1Port31 = Eth1Port0 + 31,
        [Description("IMU")]
        ImuPort0 = 3488,
        [Description("IMU_1")]
        ImuPort1 = ImuPort0 + 1,
        [Description("IMU_2")]
        ImuPort2 = ImuPort0 + 2,
        [Description("IMU_3")]
        ImuPort3 = ImuPort0 + 3,
        [Description("IMU_4")]
        ImuPort4 = ImuPort0 + 4,
        [Description("IMU_5")]
        ImuPort5 = ImuPort0 + 5,
        [Description("IMU_6")]
        ImuPort6 = ImuPort0 + 6,
        [Description("IMU_7")]
        ImuPort7 = ImuPort0 + 7,
        [Description("IMU_8")]
        ImuPort8 = ImuPort0 + 8,
        [Description("IMU_9")]
        ImuPort9 = ImuPort0 + 9,
        [Description("IMU_10")]
        ImuPort10 = ImuPort0 + 10,
        [Description("IMU_11")]
        ImuPort11 = ImuPort0 + 11,
        [Description("IMU_12")]
        ImuPort12 = ImuPort0 + 12,
        [Description("IMU_13")]
        ImuPort13 = ImuPort0 + 13,
        [Description("IMU_14")]
        ImuPort14 = ImuPort0 + 14,
        [Description("IMU_15")]
        ImuPort15 = ImuPort0 + 15,
        [Description("IMU_16")]
        ImuPort16 = ImuPort0 + 16,
        [Description("IMU_17")]
        ImuPort17 = ImuPort0 + 17,
        [Description("IMU_18")]
        ImuPort18 = ImuPort0 + 18,
        [Description("IMU_19")]
        ImuPort19 = ImuPort0 + 19,
        [Description("IMU_20")]
        ImuPort20 = ImuPort0 + 20,
        [Description("IMU_21")]
        ImuPort21 = ImuPort0 + 21,
        [Description("IMU_22")]
        ImuPort22 = ImuPort0 + 22,
        [Description("IMU_23")]
        ImuPort23 = ImuPort0 + 23,
        [Description("IMU_24")]
        ImuPort24 = ImuPort0 + 24,
        [Description("IMU_25")]
        ImuPort25 = ImuPort0 + 25,
        [Description("IMU_26")]
        ImuPort26 = ImuPort0 + 26,
        [Description("IMU_27")]
        ImuPort27 = ImuPort0 + 27,
        [Description("IMU_28")]
        ImuPort28 = ImuPort0 + 28,
        [Description("IMU_29")]
        ImuPort29 = ImuPort0 + 29,
        [Description("IMU_30")]
        ImuPort30 = ImuPort0 + 30,
        [Description("IMU_31")]
        ImuPort31 = ImuPort0 + 31,
        [Description("ICOM1")]
        ICom1Port0 = 4000,
        [Description("ICOM1_1")]
        ICom1Port1 = ICom1Port0 + 1,
        [Description("ICOM1_2")]
        ICom1Port2 = ICom1Port0 + 2,
        [Description("ICOM1_3")]
        ICom1Port3 = ICom1Port0 + 3,
        [Description("ICOM1_4")]
        ICom1Port4 = ICom1Port0 + 4,
        [Description("ICOM1_5")]
        ICom1Port5 = ICom1Port0 + 5,
        [Description("ICOM1_6")]
        ICom1Port6 = ICom1Port0 + 6,
        [Description("ICOM1_7")]
        ICom1Port7 = ICom1Port0 + 7,
        [Description("ICOM1_8")]
        ICom1Port8 = ICom1Port0 + 8,
        [Description("ICOM1_9")]
        ICom1Port9 = ICom1Port0 + 9,
        [Description("ICOM1_10")]
        ICom1Port10 = ICom1Port0 + 10,
        [Description("ICOM1_11")]
        ICom1Port11 = ICom1Port0 + 11,
        [Description("ICOM1_12")]
        ICom1Port12 = ICom1Port0 + 12,
        [Description("ICOM1_13")]
        ICom1Port13 = ICom1Port0 + 13,
        [Description("ICOM1_14")]
        ICom1Port14 = ICom1Port0 + 14,
        [Description("ICOM1_15")]
        ICom1Port15 = ICom1Port0 + 15,
        [Description("ICOM1_16")]
        ICom1Port16 = ICom1Port0 + 16,
        [Description("ICOM1_17")]
        ICom1Port17 = ICom1Port0 + 17,
        [Description("ICOM1_18")]
        ICom1Port18 = ICom1Port0 + 18,
        [Description("ICOM1_19")]
        ICom1Port19 = ICom1Port0 + 19,
        [Description("ICOM1_20")]
        ICom1Port20 = ICom1Port0 + 20,
        [Description("ICOM1_21")]
        ICom1Port21 = ICom1Port0 + 21,
        [Description("ICOM1_22")]
        ICom1Port22 = ICom1Port0 + 22,
        [Description("ICOM1_23")]
        ICom1Port23 = ICom1Port0 + 23,
        [Description("ICOM1_24")]
        ICom1Port24 = ICom1Port0 + 24,
        [Description("ICOM1_25")]
        ICom1Port25 = ICom1Port0 + 25,
        [Description("ICOM1_26")]
        ICom1Port26 = ICom1Port0 + 26,
        [Description("ICOM1_27")]
        ICom1Port27 = ICom1Port0 + 27,
        [Description("ICOM1_28")]
        ICom1Port28 = ICom1Port0 + 28,
        [Description("ICOM1_29")]
        ICom1Port29 = ICom1Port0 + 29,
        [Description("ICOM1_30")]
        ICom1Port30 = ICom1Port0 + 30,
        [Description("ICOM1_31")]
        ICom1Port31 = ICom1Port0 + 31,
        [Description("ICOM2")]
        ICom2Port0 = 4256,
        [Description("ICOM2_1")]
        ICom2Port1 = ICom2Port0 + 1,
        [Description("ICOM2_2")]
        ICom2Port2 = ICom2Port0 + 2,
        [Description("ICOM2_3")]
        ICom2Port3 = ICom2Port0 + 3,
        [Description("ICOM2_4")]
        ICom2Port4 = ICom2Port0 + 4,
        [Description("ICOM2_5")]
        ICom2Port5 = ICom2Port0 + 5,
        [Description("ICOM2_6")]
        ICom2Port6 = ICom2Port0 + 6,
        [Description("ICOM2_7")]
        ICom2Port7 = ICom2Port0 + 7,
        [Description("ICOM2_8")]
        ICom2Port8 = ICom2Port0 + 8,
        [Description("ICOM2_9")]
        ICom2Port9 = ICom2Port0 + 9,
        [Description("ICOM2_10")]
        ICom2Port10 = ICom2Port0 + 10,
        [Description("ICOM2_11")]
        ICom2Port11 = ICom2Port0 + 11,
        [Description("ICOM2_12")]
        ICom2Port12 = ICom2Port0 + 12,
        [Description("ICOM2_13")]
        ICom2Port13 = ICom2Port0 + 13,
        [Description("ICOM2_14")]
        ICom2Port14 = ICom2Port0 + 14,
        [Description("ICOM2_15")]
        ICom2Port15 = ICom2Port0 + 15,
        [Description("ICOM2_16")]
        ICom2Port16 = ICom2Port0 + 16,
        [Description("ICOM2_17")]
        ICom2Port17 = ICom2Port0 + 17,
        [Description("ICOM2_18")]
        ICom2Port18 = ICom2Port0 + 18,
        [Description("ICOM2_19")]
        ICom2Port19 = ICom2Port0 + 19,
        [Description("ICOM2_20")]
        ICom2Port20 = ICom2Port0 + 20,
        [Description("ICOM2_21")]
        ICom2Port21 = ICom2Port0 + 21,
        [Description("ICOM2_22")]
        ICom2Port22 = ICom2Port0 + 22,
        [Description("ICOM2_23")]
        ICom2Port23 = ICom2Port0 + 23,
        [Description("ICOM2_24")]
        ICom2Port24 = ICom2Port0 + 24,
        [Description("ICOM2_25")]
        ICom2Port25 = ICom2Port0 + 25,
        [Description("ICOM2_26")]
        ICom2Port26 = ICom2Port0 + 26,
        [Description("ICOM2_27")]
        ICom2Port27 = ICom2Port0 + 27,
        [Description("ICOM2_28")]
        ICom2Port28 = ICom2Port0 + 28,
        [Description("ICOM2_29")]
        ICom2Port29 = ICom2Port0 + 29,
        [Description("ICOM2_30")]
        ICom2Port30 = ICom2Port0 + 30,
        [Description("ICOM2_31")]
        ICom2Port31 = ICom2Port0 + 31,
        [Description("ICOM3")]
        ICom3Port0 = 4512,
        [Description("ICOM3_1")]
        ICom3Port1 = ICom3Port0 + 1,
        [Description("ICOM3_2")]
        ICom3Port2 = ICom3Port0 + 2,
        [Description("ICOM3_3")]
        ICom3Port3 = ICom3Port0 + 3,
        [Description("ICOM3_4")]
        ICom3Port4 = ICom3Port0 + 4,
        [Description("ICOM3_5")]
        ICom3Port5 = ICom3Port0 + 5,
        [Description("ICOM3_6")]
        ICom3Port6 = ICom3Port0 + 6,
        [Description("ICOM3_7")]
        ICom3Port7 = ICom3Port0 + 7,
        [Description("ICOM3_8")]
        ICom3Port8 = ICom3Port0 + 8,
        [Description("ICOM3_9")]
        ICom3Port9 = ICom3Port0 + 9,
        [Description("ICOM3_10")]
        ICom3Port10 = ICom3Port0 + 10,
        [Description("ICOM3_11")]
        ICom3Port11 = ICom3Port0 + 11,
        [Description("ICOM3_12")]
        ICom3Port12 = ICom3Port0 + 12,
        [Description("ICOM3_13")]
        ICom3Port13 = ICom3Port0 + 13,
        [Description("ICOM3_14")]
        ICom3Port14 = ICom3Port0 + 14,
        [Description("ICOM3_15")]
        ICom3Port15 = ICom3Port0 + 15,
        [Description("ICOM3_16")]
        ICom3Port16 = ICom3Port0 + 16,
        [Description("ICOM3_17")]
        ICom3Port17 = ICom3Port0 + 17,
        [Description("ICOM3_18")]
        ICom3Port18 = ICom3Port0 + 18,
        [Description("ICOM3_19")]
        ICom3Port19 = ICom3Port0 + 19,
        [Description("ICOM3_20")]
        ICom3Port20 = ICom3Port0 + 20,
        [Description("ICOM3_21")]
        ICom3Port21 = ICom3Port0 + 21,
        [Description("ICOM3_22")]
        ICom3Port22 = ICom3Port0 + 22,
        [Description("ICOM3_23")]
        ICom3Port23 = ICom3Port0 + 23,
        [Description("ICOM3_24")]
        ICom3Port24 = ICom3Port0 + 24,
        [Description("ICOM3_25")]
        ICom3Port25 = ICom3Port0 + 25,
        [Description("ICOM3_26")]
        ICom3Port26 = ICom3Port0 + 26,
        [Description("ICOM3_27")]
        ICom3Port27 = ICom3Port0 + 27,
        [Description("ICOM3_28")]
        ICom3Port28 = ICom3Port0 + 28,
        [Description("ICOM3_29")]
        ICom3Port29 = ICom3Port0 + 29,
        [Description("ICOM3_30")]
        ICom3Port30 = ICom3Port0 + 30,
        [Description("ICOM3_31")]
        ICom3Port31 = ICom3Port0 + 31,
        [Description("NCOM1")]
        NCom1Port0 = 4768,
        [Description("NCOM1_1")]
        NCom1Port1 = NCom1Port0 + 1,
        [Description("NCOM1_2")]
        NCom1Port2 = NCom1Port0 + 2,
        [Description("NCOM1_3")]
        NCom1Port3 = NCom1Port0 + 3,
        [Description("NCOM1_4")]
        NCom1Port4 = NCom1Port0 + 4,
        [Description("NCOM1_5")]
        NCom1Port5 = NCom1Port0 + 5,
        [Description("NCOM1_6")]
        NCom1Port6 = NCom1Port0 + 6,
        [Description("NCOM1_7")]
        NCom1Port7 = NCom1Port0 + 7,
        [Description("NCOM1_8")]
        NCom1Port8 = NCom1Port0 + 8,
        [Description("NCOM1_9")]
        NCom1Port9 = NCom1Port0 + 9,
        [Description("NCOM1_10")]
        NCom1Port10 = NCom1Port0 + 10,
        [Description("NCOM1_11")]
        NCom1Port11 = NCom1Port0 + 11,
        [Description("NCOM1_12")]
        NCom1Port12 = NCom1Port0 + 12,
        [Description("NCOM1_13")]
        NCom1Port13 = NCom1Port0 + 13,
        [Description("NCOM1_14")]
        NCom1Port14 = NCom1Port0 + 14,
        [Description("NCOM1_15")]
        NCom1Port15 = NCom1Port0 + 15,
        [Description("NCOM1_16")]
        NCom1Port16 = NCom1Port0 + 16,
        [Description("NCOM1_17")]
        NCom1Port17 = NCom1Port0 + 17,
        [Description("NCOM1_18")]
        NCom1Port18 = NCom1Port0 + 18,
        [Description("NCOM1_19")]
        NCom1Port19 = NCom1Port0 + 19,
        [Description("NCOM1_20")]
        NCom1Port20 = NCom1Port0 + 20,
        [Description("NCOM1_21")]
        NCom1Port21 = NCom1Port0 + 21,
        [Description("NCOM1_22")]
        NCom1Port22 = NCom1Port0 + 22,
        [Description("NCOM1_23")]
        NCom1Port23 = NCom1Port0 + 23,
        [Description("NCOM1_24")]
        NCom1Port24 = NCom1Port0 + 24,
        [Description("NCOM1_25")]
        NCom1Port25 = NCom1Port0 + 25,
        [Description("NCOM1_26")]
        NCom1Port26 = NCom1Port0 + 26,
        [Description("NCOM1_27")]
        NCom1Port27 = NCom1Port0 + 27,
        [Description("NCOM1_28")]
        NCom1Port28 = NCom1Port0 + 28,
        [Description("NCOM1_29")]
        NCom1Port29 = NCom1Port0 + 29,
        [Description("NCOM1_30")]
        NCom1Port30 = NCom1Port0 + 30,
        [Description("NCOM1_31")]
        NCom1Port31 = NCom1Port0 + 31,
        [Description("NCOM2")]
        NCom2Port0 = 5024,
        [Description("NCOM2_1")]
        NCom2Port1 = NCom2Port0 + 1,
        [Description("NCOM2_2")]
        NCom2Port2 = NCom2Port0 + 2,
        [Description("NCOM2_3")]
        NCom2Port3 = NCom2Port0 + 3,
        [Description("NCOM2_4")]
        NCom2Port4 = NCom2Port0 + 4,
        [Description("NCOM2_5")]
        NCom2Port5 = NCom2Port0 + 5,
        [Description("NCOM2_6")]
        NCom2Port6 = NCom2Port0 + 6,
        [Description("NCOM2_7")]
        NCom2Port7 = NCom2Port0 + 7,
        [Description("NCOM2_8")]
        NCom2Port8 = NCom2Port0 + 8,
        [Description("NCOM2_9")]
        NCom2Port9 = NCom2Port0 + 9,
        [Description("NCOM2_10")]
        NCom2Port10 = NCom2Port0 + 10,
        [Description("NCOM2_11")]
        NCom2Port11 = NCom2Port0 + 11,
        [Description("NCOM2_12")]
        NCom2Port12 = NCom2Port0 + 12,
        [Description("NCOM2_13")]
        NCom2Port13 = NCom2Port0 + 13,
        [Description("NCOM2_14")]
        NCom2Port14 = NCom2Port0 + 14,
        [Description("NCOM2_15")]
        NCom2Port15 = NCom2Port0 + 15,
        [Description("NCOM2_16")]
        NCom2Port16 = NCom2Port0 + 16,
        [Description("NCOM2_17")]
        NCom2Port17 = NCom2Port0 + 17,
        [Description("NCOM2_18")]
        NCom2Port18 = NCom2Port0 + 18,
        [Description("NCOM2_19")]
        NCom2Port19 = NCom2Port0 + 19,
        [Description("NCOM2_20")]
        NCom2Port20 = NCom2Port0 + 20,
        [Description("NCOM2_21")]
        NCom2Port21 = NCom2Port0 + 21,
        [Description("NCOM2_22")]
        NCom2Port22 = NCom2Port0 + 22,
        [Description("NCOM2_23")]
        NCom2Port23 = NCom2Port0 + 23,
        [Description("NCOM2_24")]
        NCom2Port24 = NCom2Port0 + 24,
        [Description("NCOM2_25")]
        NCom2Port25 = NCom2Port0 + 25,
        [Description("NCOM2_26")]
        NCom2Port26 = NCom2Port0 + 26,
        [Description("NCOM2_27")]
        NCom2Port27 = NCom2Port0 + 27,
        [Description("NCOM2_28")]
        NCom2Port28 = NCom2Port0 + 28,
        [Description("NCOM2_29")]
        NCom2Port29 = NCom2Port0 + 29,
        [Description("NCOM2_30")]
        NCom2Port30 = NCom2Port0 + 30,
        [Description("NCOM2_31")]
        NCom2Port31 = NCom2Port0 + 31,
        [Description("NCOM3")]
        NCom3Port0 = 5280,
        [Description("NCOM3_1")]
        NCom3Port1 = NCom3Port0 + 1,
        [Description("NCOM3_2")]
        NCom3Port2 = NCom3Port0 + 2,
        [Description("NCOM3_3")]
        NCom3Port3 = NCom3Port0 + 3,
        [Description("NCOM3_4")]
        NCom3Port4 = NCom3Port0 + 4,
        [Description("NCOM3_5")]
        NCom3Port5 = NCom3Port0 + 5,
        [Description("NCOM3_6")]
        NCom3Port6 = NCom3Port0 + 6,
        [Description("NCOM3_7")]
        NCom3Port7 = NCom3Port0 + 7,
        [Description("NCOM3_8")]
        NCom3Port8 = NCom3Port0 + 8,
        [Description("NCOM3_9")]
        NCom3Port9 = NCom3Port0 + 9,
        [Description("NCOM3_10")]
        NCom3Port10 = NCom3Port0 + 10,
        [Description("NCOM3_11")]
        NCom3Port11 = NCom3Port0 + 11,
        [Description("NCOM3_12")]
        NCom3Port12 = NCom3Port0 + 12,
        [Description("NCOM3_13")]
        NCom3Port13 = NCom3Port0 + 13,
        [Description("NCOM3_14")]
        NCom3Port14 = NCom3Port0 + 14,
        [Description("NCOM3_15")]
        NCom3Port15 = NCom3Port0 + 15,
        [Description("NCOM3_16")]
        NCom3Port16 = NCom3Port0 + 16,
        [Description("NCOM3_17")]
        NCom3Port17 = NCom3Port0 + 17,
        [Description("NCOM3_18")]
        NCom3Port18 = NCom3Port0 + 18,
        [Description("NCOM3_19")]
        NCom3Port19 = NCom3Port0 + 19,
        [Description("NCOM3_20")]
        NCom3Port20 = NCom3Port0 + 20,
        [Description("NCOM3_21")]
        NCom3Port21 = NCom3Port0 + 21,
        [Description("NCOM3_22")]
        NCom3Port22 = NCom3Port0 + 22,
        [Description("NCOM3_23")]
        NCom3Port23 = NCom3Port0 + 23,
        [Description("NCOM3_24")]
        NCom3Port24 = NCom3Port0 + 24,
        [Description("NCOM3_25")]
        NCom3Port25 = NCom3Port0 + 25,
        [Description("NCOM3_26")]
        NCom3Port26 = NCom3Port0 + 26,
        [Description("NCOM3_27")]
        NCom3Port27 = NCom3Port0 + 27,
        [Description("NCOM3_28")]
        NCom3Port28 = NCom3Port0 + 28,
        [Description("NCOM3_29")]
        NCom3Port29 = NCom3Port0 + 29,
        [Description("NCOM3_30")]
        NCom3Port30 = NCom3Port0 + 30,
        [Description("NCOM3_31")]
        NCom3Port31 = NCom3Port0 + 31,
        [Description("ICOM4")]
        ICom4Port0 = 5536,
        [Description("ICOM4_1")]
        ICom4Port1 = ICom4Port0 + 1,
        [Description("ICOM4_2")]
        ICom4Port2 = ICom4Port0 + 2,
        [Description("ICOM4_3")]
        ICom4Port3 = ICom4Port0 + 3,
        [Description("ICOM4_4")]
        ICom4Port4 = ICom4Port0 + 4,
        [Description("ICOM4_5")]
        ICom4Port5 = ICom4Port0 + 5,
        [Description("ICOM4_6")]
        ICom4Port6 = ICom4Port0 + 6,
        [Description("ICOM4_7")]
        ICom4Port7 = ICom4Port0 + 7,
        [Description("ICOM4_8")]
        ICom4Port8 = ICom4Port0 + 8,
        [Description("ICOM4_9")]
        ICom4Port9 = ICom4Port0 + 9,
        [Description("ICOM4_10")]
        ICom4Port10 = ICom4Port0 + 10,
        [Description("ICOM4_11")]
        ICom4Port11 = ICom4Port0 + 11,
        [Description("ICOM4_12")]
        ICom4Port12 = ICom4Port0 + 12,
        [Description("ICOM4_13")]
        ICom4Port13 = ICom4Port0 + 13,
        [Description("ICOM4_14")]
        ICom4Port14 = ICom4Port0 + 14,
        [Description("ICOM4_15")]
        ICom4Port15 = ICom4Port0 + 15,
        [Description("ICOM4_16")]
        ICom4Port16 = ICom4Port0 + 16,
        [Description("ICOM4_17")]
        ICom4Port17 = ICom4Port0 + 17,
        [Description("ICOM4_18")]
        ICom4Port18 = ICom4Port0 + 18,
        [Description("ICOM4_19")]
        ICom4Port19 = ICom4Port0 + 19,
        [Description("ICOM4_20")]
        ICom4Port20 = ICom4Port0 + 20,
        [Description("ICOM4_21")]
        ICom4Port21 = ICom4Port0 + 21,
        [Description("ICOM4_22")]
        ICom4Port22 = ICom4Port0 + 22,
        [Description("ICOM4_23")]
        ICom4Port23 = ICom4Port0 + 23,
        [Description("ICOM4_24")]
        ICom4Port24 = ICom4Port0 + 24,
        [Description("ICOM4_25")]
        ICom4Port25 = ICom4Port0 + 25,
        [Description("ICOM4_26")]
        ICom4Port26 = ICom4Port0 + 26,
        [Description("ICOM4_27")]
        ICom4Port27 = ICom4Port0 + 27,
        [Description("ICOM4_28")]
        ICom4Port28 = ICom4Port0 + 28,
        [Description("ICOM4_29")]
        ICom4Port29 = ICom4Port0 + 29,
        [Description("ICOM4_30")]
        ICom4Port30 = ICom4Port0 + 30,
        [Description("ICOM4_31")]
        ICom4Port31 = ICom4Port0 + 31,
        [Description("WCOM1")]
        WCom1Port0 = 5792,
        [Description("WCOM1_1")]
        WCom1Port1 = WCom1Port0 + 1,
        [Description("WCOM1_2")]
        WCom1Port2 = WCom1Port0 + 2,
        [Description("WCOM1_3")]
        WCom1Port3 = WCom1Port0 + 3,
        [Description("WCOM1_4")]
        WCom1Port4 = WCom1Port0 + 4,
        [Description("WCOM1_5")]
        WCom1Port5 = WCom1Port0 + 5,
        [Description("WCOM1_6")]
        WCom1Port6 = WCom1Port0 + 6,
        [Description("WCOM1_7")]
        WCom1Port7 = WCom1Port0 + 7,
        [Description("WCOM1_8")]
        WCom1Port8 = WCom1Port0 + 8,
        [Description("WCOM1_9")]
        WCom1Port9 = WCom1Port0 + 9,
        [Description("WCOM1_10")]
        WCom1Port10 = WCom1Port0 + 10,
        [Description("WCOM1_11")]
        WCom1Port11 = WCom1Port0 + 11,
        [Description("WCOM1_12")]
        WCom1Port12 = WCom1Port0 + 12,
        [Description("WCOM1_13")]
        WCom1Port13 = WCom1Port0 + 13,
        [Description("WCOM1_14")]
        WCom1Port14 = WCom1Port0 + 14,
        [Description("WCOM1_15")]
        WCom1Port15 = WCom1Port0 + 15,
        [Description("WCOM1_16")]
        WCom1Port16 = WCom1Port0 + 16,
        [Description("WCOM1_17")]
        WCom1Port17 = WCom1Port0 + 17,
        [Description("WCOM1_18")]
        WCom1Port18 = WCom1Port0 + 18,
        [Description("WCOM1_19")]
        WCom1Port19 = WCom1Port0 + 19,
        [Description("WCOM1_20")]
        WCom1Port20 = WCom1Port0 + 20,
        [Description("WCOM1_21")]
        WCom1Port21 = WCom1Port0 + 21,
        [Description("WCOM1_22")]
        WCom1Port22 = WCom1Port0 + 22,
        [Description("WCOM1_23")]
        WCom1Port23 = WCom1Port0 + 23,
        [Description("WCOM1_24")]
        WCom1Port24 = WCom1Port0 + 24,
        [Description("WCOM1_25")]
        WCom1Port25 = WCom1Port0 + 25,
        [Description("WCOM1_26")]
        WCom1Port26 = WCom1Port0 + 26,
        [Description("WCOM1_27")]
        WCom1Port27 = WCom1Port0 + 27,
        [Description("WCOM1_28")]
        WCom1Port28 = WCom1Port0 + 28,
        [Description("WCOM1_29")]
        WCom1Port29 = WCom1Port0 + 29,
        [Description("WCOM1_30")]
        WCom1Port30 = WCom1Port0 + 30,
        [Description("WCOM1_31")]
        WCom1Port31 = WCom1Port0 + 31,
        [Description("COM5_ALL")]
        Com5All = 5824,
        [Description("COM6_ALL")]
        Com6All = 5825,
        [Description("BT1_ALL")]
        Bluetooth1All = 5826,
        [Description("COM7_ALL")]
        Com7All = 5827,
        [Description("COM8_ALL")]
        Com8All = 5828,
        [Description("COM9_ALL")]
        Com9All = 5829,
        [Description("COM10_ALL")]
        Com10All = 5830,
        [Description("CCOM1_ALL")]
        CCom1All = 5831,
        [Description("CCOM2_ALL")]
        CCom2All = 5832,
        [Description("CCOM3_ALL")]
        CCom3All = 5833,
        [Description("CCOM4_ALL")]
        CCom4All = 5834,
        [Description("CCOM5_ALL")]
        CCom5All = 5835,
        [Description("CCOM6_ALL")]
        CCom6All = 5836,
        [Description("ICOM5_ALL")]
        ICom5All = 5839,
        [Description("ICOM6_ALL")]
        ICom6All = 5840,
        [Description("ICOM7_ALL")]
        ICom7All = 5841,
        [Description("SCOM1_ALL")]
        SCom1All = 5842,
        [Description("SCOM2_ALL")]
        SCom2All = 5843,
        [Description("SCOM3_ALL")]
        SCom3All = 5844,
        [Description("SCOM4_ALL")]
        SCom4All = 5845,
        [Description("COM5")]
        Com5Port0 = 6049,
        [Description("COM5_1")]
        Com5Port1 = Com5Port0 + 1,
        [Description("COM5_2")]
        Com5Port2 = Com5Port0 + 2,
        [Description("COM5_3")]
        Com5Port3 = Com5Port0 + 3,
        [Description("COM5_4")]
        Com5Port4 = Com5Port0 + 4,
        [Description("COM5_5")]
        Com5Port5 = Com5Port0 + 5,
        [Description("COM5_6")]
        Com5Port6 = Com5Port0 + 6,
        [Description("COM5_7")]
        Com5Port7 = Com5Port0 + 7,
        [Description("COM5_8")]
        Com5Port8 = Com5Port0 + 8,
        [Description("COM5_9")]
        Com5Port9 = Com5Port0 + 9,
        [Description("COM5_10")]
        Com5Port10 = Com5Port0 + 10,
        [Description("COM5_11")]
        Com5Port11 = Com5Port0 + 11,
        [Description("COM5_12")]
        Com5Port12 = Com5Port0 + 12,
        [Description("COM5_13")]
        Com5Port13 = Com5Port0 + 13,
        [Description("COM5_14")]
        Com5Port14 = Com5Port0 + 14,
        [Description("COM5_15")]
        Com5Port15 = Com5Port0 + 15,
        [Description("COM5_16")]
        Com5Port16 = Com5Port0 + 16,
        [Description("COM5_17")]
        Com5Port17 = Com5Port0 + 17,
        [Description("COM5_18")]
        Com5Port18 = Com5Port0 + 18,
        [Description("COM5_19")]
        Com5Port19 = Com5Port0 + 19,
        [Description("COM5_20")]
        Com5Port20 = Com5Port0 + 20,
        [Description("COM5_21")]
        Com5Port21 = Com5Port0 + 21,
        [Description("COM5_22")]
        Com5Port22 = Com5Port0 + 22,
        [Description("COM5_23")]
        Com5Port23 = Com5Port0 + 23,
        [Description("COM5_24")]
        Com5Port24 = Com5Port0 + 24,
        [Description("COM5_25")]
        Com5Port25 = Com5Port0 + 25,
        [Description("COM5_26")]
        Com5Port26 = Com5Port0 + 26,
        [Description("COM5_27")]
        Com5Port27 = Com5Port0 + 27,
        [Description("COM5_28")]
        Com5Port28 = Com5Port0 + 28,
        [Description("COM5_29")]
        Com5Port29 = Com5Port0 + 29,
        [Description("COM5_30")]
        Com5Port30 = Com5Port0 + 30,
        [Description("COM5_31")]
        Com5Port31 = Com5Port0 + 31,
        [Description("COM6")]
        Com6Port0 = 6304,
        [Description("COM6_1")]
        Com6Port1 = Com6Port0 + 1,
        [Description("COM6_2")]
        Com6Port2 = Com6Port0 + 2,
        [Description("COM6_3")]
        Com6Port3 = Com6Port0 + 3,
        [Description("COM6_4")]
        Com6Port4 = Com6Port0 + 4,
        [Description("COM6_5")]
        Com6Port5 = Com6Port0 + 5,
        [Description("COM6_6")]
        Com6Port6 = Com6Port0 + 6,
        [Description("COM6_7")]
        Com6Port7 = Com6Port0 + 7,
        [Description("COM6_8")]
        Com6Port8 = Com6Port0 + 8,
        [Description("COM6_9")]
        Com6Port9 = Com6Port0 + 9,
        [Description("COM6_10")]
        Com6Port10 = Com6Port0 + 10,
        [Description("COM6_11")]
        Com6Port11 = Com6Port0 + 11,
        [Description("COM6_12")]
        Com6Port12 = Com6Port0 + 12,
        [Description("COM6_13")]
        Com6Port13 = Com6Port0 + 13,
        [Description("COM6_14")]
        Com6Port14 = Com6Port0 + 14,
        [Description("COM6_15")]
        Com6Port15 = Com6Port0 + 15,
        [Description("COM6_16")]
        Com6Port16 = Com6Port0 + 16,
        [Description("COM6_17")]
        Com6Port17 = Com6Port0 + 17,
        [Description("COM6_18")]
        Com6Port18 = Com6Port0 + 18,
        [Description("COM6_19")]
        Com6Port19 = Com6Port0 + 19,
        [Description("COM6_20")]
        Com6Port20 = Com6Port0 + 20,
        [Description("COM6_21")]
        Com6Port21 = Com6Port0 + 21,
        [Description("COM6_22")]
        Com6Port22 = Com6Port0 + 22,
        [Description("COM6_23")]
        Com6Port23 = Com6Port0 + 23,
        [Description("COM6_24")]
        Com6Port24 = Com6Port0 + 24,
        [Description("COM6_25")]
        Com6Port25 = Com6Port0 + 25,
        [Description("COM6_26")]
        Com6Port26 = Com6Port0 + 26,
        [Description("COM6_27")]
        Com6Port27 = Com6Port0 + 27,
        [Description("COM6_28")]
        Com6Port28 = Com6Port0 + 28,
        [Description("COM6_29")]
        Com6Port29 = Com6Port0 + 29,
        [Description("COM6_30")]
        Com6Port30 = Com6Port0 + 30,
        [Description("COM6_31")]
        Com6Port31 = Com6Port0 + 31,
        [Description("BT1")]
        Bluetooth1Port0 = 6560,
        [Description("BT1_1")]
        Bluetooth1Port1 = Bluetooth1Port0 + 1,
        [Description("BT1_2")]
        Bluetooth1Port2 = Bluetooth1Port0 + 2,
        [Description("BT1_3")]
        Bluetooth1Port3 = Bluetooth1Port0 + 3,
        [Description("BT1_4")]
        Bluetooth1Port4 = Bluetooth1Port0 + 4,
        [Description("BT1_5")]
        Bluetooth1Port5 = Bluetooth1Port0 + 5,
        [Description("BT1_6")]
        Bluetooth1Port6 = Bluetooth1Port0 + 6,
        [Description("BT1_7")]
        Bluetooth1Port7 = Bluetooth1Port0 + 7,
        [Description("BT1_8")]
        Bluetooth1Port8 = Bluetooth1Port0 + 8,
        [Description("BT1_9")]
        Bluetooth1Port9 = Bluetooth1Port0 + 9,
        [Description("BT1_10")]
        Bluetooth1Port10 = Bluetooth1Port0 + 10,
        [Description("BT1_11")]
        Bluetooth1Port11 = Bluetooth1Port0 + 11,
        [Description("BT1_12")]
        Bluetooth1Port12 = Bluetooth1Port0 + 12,
        [Description("BT1_13")]
        Bluetooth1Port13 = Bluetooth1Port0 + 13,
        [Description("BT1_14")]
        Bluetooth1Port14 = Bluetooth1Port0 + 14,
        [Description("BT1_15")]
        Bluetooth1Port15 = Bluetooth1Port0 + 15,
        [Description("BT1_16")]
        Bluetooth1Port16 = Bluetooth1Port0 + 16,
        [Description("BT1_17")]
        Bluetooth1Port17 = Bluetooth1Port0 + 17,
        [Description("BT1_18")]
        Bluetooth1Port18 = Bluetooth1Port0 + 18,
        [Description("BT1_19")]
        Bluetooth1Port19 = Bluetooth1Port0 + 19,
        [Description("BT1_20")]
        Bluetooth1Port20 = Bluetooth1Port0 + 20,
        [Description("BT1_21")]
        Bluetooth1Port21 = Bluetooth1Port0 + 21,
        [Description("BT1_22")]
        Bluetooth1Port22 = Bluetooth1Port0 + 22,
        [Description("BT1_23")]
        Bluetooth1Port23 = Bluetooth1Port0 + 23,
        [Description("BT1_24")]
        Bluetooth1Port24 = Bluetooth1Port0 + 24,
        [Description("BT1_25")]
        Bluetooth1Port25 = Bluetooth1Port0 + 25,
        [Description("BT1_26")]
        Bluetooth1Port26 = Bluetooth1Port0 + 26,
        [Description("BT1_27")]
        Bluetooth1Port27 = Bluetooth1Port0 + 27,
        [Description("BT1_28")]
        Bluetooth1Port28 = Bluetooth1Port0 + 28,
        [Description("BT1_29")]
        Bluetooth1Port29 = Bluetooth1Port0 + 29,
        [Description("BT1_30")]
        Bluetooth1Port30 = Bluetooth1Port0 + 30,
        [Description("BT1_31")]
        Bluetooth1Port31 = Bluetooth1Port0 + 31,
        [Description("COM7")]
        Com7Port0 = 6816,
        [Description("COM7_1")]
        Com7Port1 = Com7Port0 + 1,
        [Description("COM7_2")]
        Com7Port2 = Com7Port0 + 2,
        [Description("COM7_3")]
        Com7Port3 = Com7Port0 + 3,
        [Description("COM7_4")]
        Com7Port4 = Com7Port0 + 4,
        [Description("COM7_5")]
        Com7Port5 = Com7Port0 + 5,
        [Description("COM7_6")]
        Com7Port6 = Com7Port0 + 6,
        [Description("COM7_7")]
        Com7Port7 = Com7Port0 + 7,
        [Description("COM7_8")]
        Com7Port8 = Com7Port0 + 8,
        [Description("COM7_9")]
        Com7Port9 = Com7Port0 + 9,
        [Description("COM7_10")]
        Com7Port10 = Com7Port0 + 10,
        [Description("COM7_11")]
        Com7Port11 = Com7Port0 + 11,
        [Description("COM7_12")]
        Com7Port12 = Com7Port0 + 12,
        [Description("COM7_13")]
        Com7Port13 = Com7Port0 + 13,
        [Description("COM7_14")]
        Com7Port14 = Com7Port0 + 14,
        [Description("COM7_15")]
        Com7Port15 = Com7Port0 + 15,
        [Description("COM7_16")]
        Com7Port16 = Com7Port0 + 16,
        [Description("COM7_17")]
        Com7Port17 = Com7Port0 + 17,
        [Description("COM7_18")]
        Com7Port18 = Com7Port0 + 18,
        [Description("COM7_19")]
        Com7Port19 = Com7Port0 + 19,
        [Description("COM7_20")]
        Com7Port20 = Com7Port0 + 20,
        [Description("COM7_21")]
        Com7Port21 = Com7Port0 + 21,
        [Description("COM7_22")]
        Com7Port22 = Com7Port0 + 22,
        [Description("COM7_23")]
        Com7Port23 = Com7Port0 + 23,
        [Description("COM7_24")]
        Com7Port24 = Com7Port0 + 24,
        [Description("COM7_25")]
        Com7Port25 = Com7Port0 + 25,
        [Description("COM7_26")]
        Com7Port26 = Com7Port0 + 26,
        [Description("COM7_27")]
        Com7Port27 = Com7Port0 + 27,
        [Description("COM7_28")]
        Com7Port28 = Com7Port0 + 28,
        [Description("COM7_29")]
        Com7Port29 = Com7Port0 + 29,
        [Description("COM7_30")]
        Com7Port30 = Com7Port0 + 30,
        [Description("COM7_31")]
        Com7Port31 = Com7Port0 + 31,
        [Description("COM8")]
        Com8Port0 = 7072,
        [Description("COM8_1")]
        Com8Port1 = Com8Port0 + 1,
        [Description("COM8_2")]
        Com8Port2 = Com8Port0 + 2,
        [Description("COM8_3")]
        Com8Port3 = Com8Port0 + 3,
        [Description("COM8_4")]
        Com8Port4 = Com8Port0 + 4,
        [Description("COM8_5")]
        Com8Port5 = Com8Port0 + 5,
        [Description("COM8_6")]
        Com8Port6 = Com8Port0 + 6,
        [Description("COM8_7")]
        Com8Port7 = Com8Port0 + 7,
        [Description("COM8_8")]
        Com8Port8 = Com8Port0 + 8,
        [Description("COM8_9")]
        Com8Port9 = Com8Port0 + 9,
        [Description("COM8_10")]
        Com8Port10 = Com8Port0 + 10,
        [Description("COM8_11")]
        Com8Port11 = Com8Port0 + 11,
        [Description("COM8_12")]
        Com8Port12 = Com8Port0 + 12,
        [Description("COM8_13")]
        Com8Port13 = Com8Port0 + 13,
        [Description("COM8_14")]
        Com8Port14 = Com8Port0 + 14,
        [Description("COM8_15")]
        Com8Port15 = Com8Port0 + 15,
        [Description("COM8_16")]
        Com8Port16 = Com8Port0 + 16,
        [Description("COM8_17")]
        Com8Port17 = Com8Port0 + 17,
        [Description("COM8_18")]
        Com8Port18 = Com8Port0 + 18,
        [Description("COM8_19")]
        Com8Port19 = Com8Port0 + 19,
        [Description("COM8_20")]
        Com8Port20 = Com8Port0 + 20,
        [Description("COM8_21")]
        Com8Port21 = Com8Port0 + 21,
        [Description("COM8_22")]
        Com8Port22 = Com8Port0 + 22,
        [Description("COM8_23")]
        Com8Port23 = Com8Port0 + 23,
        [Description("COM8_24")]
        Com8Port24 = Com8Port0 + 24,
        [Description("COM8_25")]
        Com8Port25 = Com8Port0 + 25,
        [Description("COM8_26")]
        Com8Port26 = Com8Port0 + 26,
        [Description("COM8_27")]
        Com8Port27 = Com8Port0 + 27,
        [Description("COM8_28")]
        Com8Port28 = Com8Port0 + 28,
        [Description("COM8_29")]
        Com8Port29 = Com8Port0 + 29,
        [Description("COM8_30")]
        Com8Port30 = Com8Port0 + 30,
        [Description("COM8_31")]
        Com8Port31 = Com8Port0 + 31,
        [Description("COM9")]
        Com9Port0 = 7328,
        [Description("COM9_1")]
        Com9Port1 = Com9Port0 + 1,
        [Description("COM9_2")]
        Com9Port2 = Com9Port0 + 2,
        [Description("COM9_3")]
        Com9Port3 = Com9Port0 + 3,
        [Description("COM9_4")]
        Com9Port4 = Com9Port0 + 4,
        [Description("COM9_5")]
        Com9Port5 = Com9Port0 + 5,
        [Description("COM9_6")]
        Com9Port6 = Com9Port0 + 6,
        [Description("COM9_7")]
        Com9Port7 = Com9Port0 + 7,
        [Description("COM9_8")]
        Com9Port8 = Com9Port0 + 8,
        [Description("COM9_9")]
        Com9Port9 = Com9Port0 + 9,
        [Description("COM9_10")]
        Com9Port10 = Com9Port0 + 10,
        [Description("COM9_11")]
        Com9Port11 = Com9Port0 + 11,
        [Description("COM9_12")]
        Com9Port12 = Com9Port0 + 12,
        [Description("COM9_13")]
        Com9Port13 = Com9Port0 + 13,
        [Description("COM9_14")]
        Com9Port14 = Com9Port0 + 14,
        [Description("COM9_15")]
        Com9Port15 = Com9Port0 + 15,
        [Description("COM9_16")]
        Com9Port16 = Com9Port0 + 16,
        [Description("COM9_17")]
        Com9Port17 = Com9Port0 + 17,
        [Description("COM9_18")]
        Com9Port18 = Com9Port0 + 18,
        [Description("COM9_19")]
        Com9Port19 = Com9Port0 + 19,
        [Description("COM9_20")]
        Com9Port20 = Com9Port0 + 20,
        [Description("COM9_21")]
        Com9Port21 = Com9Port0 + 21,
        [Description("COM9_22")]
        Com9Port22 = Com9Port0 + 22,
        [Description("COM9_23")]
        Com9Port23 = Com9Port0 + 23,
        [Description("COM9_24")]
        Com9Port24 = Com9Port0 + 24,
        [Description("COM9_25")]
        Com9Port25 = Com9Port0 + 25,
        [Description("COM9_26")]
        Com9Port26 = Com9Port0 + 26,
        [Description("COM9_27")]
        Com9Port27 = Com9Port0 + 27,
        [Description("COM9_28")]
        Com9Port28 = Com9Port0 + 28,
        [Description("COM9_29")]
        Com9Port29 = Com9Port0 + 29,
        [Description("COM9_30")]
        Com9Port30 = Com9Port0 + 30,
        [Description("COM9_31")]
        Com9Port31 = Com9Port0 + 31,
        [Description("COM10")]
        Com10Port0 = 7584,
        [Description("COM10_1")]
        Com10Port1 = Com10Port0 + 1,
        [Description("COM10_2")]
        Com10Port2 = Com10Port0 + 2,
        [Description("COM10_3")]
        Com10Port3 = Com10Port0 + 3,
        [Description("COM10_4")]
        Com10Port4 = Com10Port0 + 4,
        [Description("COM10_5")]
        Com10Port5 = Com10Port0 + 5,
        [Description("COM10_6")]
        Com10Port6 = Com10Port0 + 6,
        [Description("COM10_7")]
        Com10Port7 = Com10Port0 + 7,
        [Description("COM10_8")]
        Com10Port8 = Com10Port0 + 8,
        [Description("COM10_9")]
        Com10Port9 = Com10Port0 + 9,
        [Description("COM10_10")]
        Com10Port10 = Com10Port0 + 10,
        [Description("COM10_11")]
        Com10Port11 = Com10Port0 + 11,
        [Description("COM10_12")]
        Com10Port12 = Com10Port0 + 12,
        [Description("COM10_13")]
        Com10Port13 = Com10Port0 + 13,
        [Description("COM10_14")]
        Com10Port14 = Com10Port0 + 14,
        [Description("COM10_15")]
        Com10Port15 = Com10Port0 + 15,
        [Description("COM10_16")]
        Com10Port16 = Com10Port0 + 16,
        [Description("COM10_17")]
        Com10Port17 = Com10Port0 + 17,
        [Description("COM10_18")]
        Com10Port18 = Com10Port0 + 18,
        [Description("COM10_19")]
        Com10Port19 = Com10Port0 + 19,
        [Description("COM10_20")]
        Com10Port20 = Com10Port0 + 20,
        [Description("COM10_21")]
        Com10Port21 = Com10Port0 + 21,
        [Description("COM10_22")]
        Com10Port22 = Com10Port0 + 22,
        [Description("COM10_23")]
        Com10Port23 = Com10Port0 + 23,
        [Description("COM10_24")]
        Com10Port24 = Com10Port0 + 24,
        [Description("COM10_25")]
        Com10Port25 = Com10Port0 + 25,
        [Description("COM10_26")]
        Com10Port26 = Com10Port0 + 26,
        [Description("COM10_27")]
        Com10Port27 = Com10Port0 + 27,
        [Description("COM10_28")]
        Com10Port28 = Com10Port0 + 28,
        [Description("COM10_29")]
        Com10Port29 = Com10Port0 + 29,
        [Description("COM10_30")]
        Com10Port30 = Com10Port0 + 30,
        [Description("COM10_31")]
        Com10Port31 = Com10Port0 + 31,
        [Description("CCOM1")]
        CCom1Port0 = 7841,
        [Description("CCOM1_1")]
        CCom1Port1 = CCom1Port0 + 1,
        [Description("CCOM1_2")]
        CCom1Port2 = CCom1Port0 + 2,
        [Description("CCOM1_3")]
        CCom1Port3 = CCom1Port0 + 3,
        [Description("CCOM1_4")]
        CCom1Port4 = CCom1Port0 + 4,
        [Description("CCOM1_5")]
        CCom1Port5 = CCom1Port0 + 5,
        [Description("CCOM1_6")]
        CCom1Port6 = CCom1Port0 + 6,
        [Description("CCOM1_7")]
        CCom1Port7 = CCom1Port0 + 7,
        [Description("CCOM1_8")]
        CCom1Port8 = CCom1Port0 + 8,
        [Description("CCOM1_9")]
        CCom1Port9 = CCom1Port0 + 9,
        [Description("CCOM1_10")]
        CCom1Port10 = CCom1Port0 + 10,
        [Description("CCOM1_11")]
        CCom1Port11 = CCom1Port0 + 11,
        [Description("CCOM1_12")]
        CCom1Port12 = CCom1Port0 + 12,
        [Description("CCOM1_13")]
        CCom1Port13 = CCom1Port0 + 13,
        [Description("CCOM1_14")]
        CCom1Port14 = CCom1Port0 + 14,
        [Description("CCOM1_15")]
        CCom1Port15 = CCom1Port0 + 15,
        [Description("CCOM1_16")]
        CCom1Port16 = CCom1Port0 + 16,
        [Description("CCOM1_17")]
        CCom1Port17 = CCom1Port0 + 17,
        [Description("CCOM1_18")]
        CCom1Port18 = CCom1Port0 + 18,
        [Description("CCOM1_19")]
        CCom1Port19 = CCom1Port0 + 19,
        [Description("CCOM1_20")]
        CCom1Port20 = CCom1Port0 + 20,
        [Description("CCOM1_21")]
        CCom1Port21 = CCom1Port0 + 21,
        [Description("CCOM1_22")]
        CCom1Port22 = CCom1Port0 + 22,
        [Description("CCOM1_23")]
        CCom1Port23 = CCom1Port0 + 23,
        [Description("CCOM1_24")]
        CCom1Port24 = CCom1Port0 + 24,
        [Description("CCOM1_25")]
        CCom1Port25 = CCom1Port0 + 25,
        [Description("CCOM1_26")]
        CCom1Port26 = CCom1Port0 + 26,
        [Description("CCOM1_27")]
        CCom1Port27 = CCom1Port0 + 27,
        [Description("CCOM1_28")]
        CCom1Port28 = CCom1Port0 + 28,
        [Description("CCOM1_29")]
        CCom1Port29 = CCom1Port0 + 29,
        [Description("CCOM1_30")]
        CCom1Port30 = CCom1Port0 + 30,
        [Description("CCOM1_31")]
        CCom1Port31 = CCom1Port0 + 31,
        [Description("CCOM2")]
        CCom2Port0 = 8127,
        [Description("CCOM2_1")]
        CCom2Port1 = CCom2Port0 + 1,
        [Description("CCOM2_2")]
        CCom2Port2 = CCom2Port0 + 2,
        [Description("CCOM2_3")]
        CCom2Port3 = CCom2Port0 + 3,
        [Description("CCOM2_4")]
        CCom2Port4 = CCom2Port0 + 4,
        [Description("CCOM2_5")]
        CCom2Port5 = CCom2Port0 + 5,
        [Description("CCOM2_6")]
        CCom2Port6 = CCom2Port0 + 6,
        [Description("CCOM2_7")]
        CCom2Port7 = CCom2Port0 + 7,
        [Description("CCOM2_8")]
        CCom2Port8 = CCom2Port0 + 8,
        [Description("CCOM2_9")]
        CCom2Port9 = CCom2Port0 + 9,
        [Description("CCOM2_10")]
        CCom2Port10 = CCom2Port0 + 10,
        [Description("CCOM2_11")]
        CCom2Port11 = CCom2Port0 + 11,
        [Description("CCOM2_12")]
        CCom2Port12 = CCom2Port0 + 12,
        [Description("CCOM2_13")]
        CCom2Port13 = CCom2Port0 + 13,
        [Description("CCOM2_14")]
        CCom2Port14 = CCom2Port0 + 14,
        [Description("CCOM2_15")]
        CCom2Port15 = CCom2Port0 + 15,
        [Description("CCOM2_16")]
        CCom2Port16 = CCom2Port0 + 16,
        [Description("CCOM2_17")]
        CCom2Port17 = CCom2Port0 + 17,
        [Description("CCOM2_18")]
        CCom2Port18 = CCom2Port0 + 18,
        [Description("CCOM2_19")]
        CCom2Port19 = CCom2Port0 + 19,
        [Description("CCOM2_20")]
        CCom2Port20 = CCom2Port0 + 20,
        [Description("CCOM2_21")]
        CCom2Port21 = CCom2Port0 + 21,
        [Description("CCOM2_22")]
        CCom2Port22 = CCom2Port0 + 22,
        [Description("CCOM2_23")]
        CCom2Port23 = CCom2Port0 + 23,
        [Description("CCOM2_24")]
        CCom2Port24 = CCom2Port0 + 24,
        [Description("CCOM2_25")]
        CCom2Port25 = CCom2Port0 + 25,
        [Description("CCOM2_26")]
        CCom2Port26 = CCom2Port0 + 26,
        [Description("CCOM2_27")]
        CCom2Port27 = CCom2Port0 + 27,
        [Description("CCOM2_28")]
        CCom2Port28 = CCom2Port0 + 28,
        [Description("CCOM2_29")]
        CCom2Port29 = CCom2Port0 + 29,
        [Description("CCOM2_30")]
        CCom2Port30 = CCom2Port0 + 30,
        [Description("CCOM2_31")]
        CCom2Port31 = CCom2Port0 + 31,
        [Description("CCOM3")]
        CCom3Port0 = 8352,
        [Description("CCOM3_1")]
        CCom3Port1 = CCom3Port0 + 1,
        [Description("CCOM3_2")]
        CCom3Port2 = CCom3Port0 + 2,
        [Description("CCOM3_3")]
        CCom3Port3 = CCom3Port0 + 3,
        [Description("CCOM3_4")]
        CCom3Port4 = CCom3Port0 + 4,
        [Description("CCOM3_5")]
        CCom3Port5 = CCom3Port0 + 5,
        [Description("CCOM3_6")]
        CCom3Port6 = CCom3Port0 + 6,
        [Description("CCOM3_7")]
        CCom3Port7 = CCom3Port0 + 7,
        [Description("CCOM3_8")]
        CCom3Port8 = CCom3Port0 + 8,
        [Description("CCOM3_9")]
        CCom3Port9 = CCom3Port0 + 9,
        [Description("CCOM3_10")]
        CCom3Port10 = CCom3Port0 + 10,
        [Description("CCOM3_11")]
        CCom3Port11 = CCom3Port0 + 11,
        [Description("CCOM3_12")]
        CCom3Port12 = CCom3Port0 + 12,
        [Description("CCOM3_13")]
        CCom3Port13 = CCom3Port0 + 13,
        [Description("CCOM3_14")]
        CCom3Port14 = CCom3Port0 + 14,
        [Description("CCOM3_15")]
        CCom3Port15 = CCom3Port0 + 15,
        [Description("CCOM3_16")]
        CCom3Port16 = CCom3Port0 + 16,
        [Description("CCOM3_17")]
        CCom3Port17 = CCom3Port0 + 17,
        [Description("CCOM3_18")]
        CCom3Port18 = CCom3Port0 + 18,
        [Description("CCOM3_19")]
        CCom3Port19 = CCom3Port0 + 19,
        [Description("CCOM3_20")]
        CCom3Port20 = CCom3Port0 + 20,
        [Description("CCOM3_21")]
        CCom3Port21 = CCom3Port0 + 21,
        [Description("CCOM3_22")]
        CCom3Port22 = CCom3Port0 + 22,
        [Description("CCOM3_23")]
        CCom3Port23 = CCom3Port0 + 23,
        [Description("CCOM3_24")]
        CCom3Port24 = CCom3Port0 + 24,
        [Description("CCOM3_25")]
        CCom3Port25 = CCom3Port0 + 25,
        [Description("CCOM3_26")]
        CCom3Port26 = CCom3Port0 + 26,
        [Description("CCOM3_27")]
        CCom3Port27 = CCom3Port0 + 27,
        [Description("CCOM3_28")]
        CCom3Port28 = CCom3Port0 + 28,
        [Description("CCOM3_29")]
        CCom3Port29 = CCom3Port0 + 29,
        [Description("CCOM3_30")]
        CCom3Port30 = CCom3Port0 + 30,
        [Description("CCOM3_31")]
        CCom3Port31 = CCom3Port0 + 31,
        [Description("CCOM4")]
        CCom4Port0 = 8608,
        [Description("CCOM4_1")]
        CCom4Port1 = CCom4Port0 + 1,
        [Description("CCOM4_2")]
        CCom4Port2 = CCom4Port0 + 2,
        [Description("CCOM4_3")]
        CCom4Port3 = CCom4Port0 + 3,
        [Description("CCOM4_4")]
        CCom4Port4 = CCom4Port0 + 4,
        [Description("CCOM4_5")]
        CCom4Port5 = CCom4Port0 + 5,
        [Description("CCOM4_6")]
        CCom4Port6 = CCom4Port0 + 6,
        [Description("CCOM4_7")]
        CCom4Port7 = CCom4Port0 + 7,
        [Description("CCOM4_8")]
        CCom4Port8 = CCom4Port0 + 8,
        [Description("CCOM4_9")]
        CCom4Port9 = CCom4Port0 + 9,
        [Description("CCOM4_10")]
        CCom4Port10 = CCom4Port0 + 10,
        [Description("CCOM4_11")]
        CCom4Port11 = CCom4Port0 + 11,
        [Description("CCOM4_12")]
        CCom4Port12 = CCom4Port0 + 12,
        [Description("CCOM4_13")]
        CCom4Port13 = CCom4Port0 + 13,
        [Description("CCOM4_14")]
        CCom4Port14 = CCom4Port0 + 14,
        [Description("CCOM4_15")]
        CCom4Port15 = CCom4Port0 + 15,
        [Description("CCOM4_16")]
        CCom4Port16 = CCom4Port0 + 16,
        [Description("CCOM4_17")]
        CCom4Port17 = CCom4Port0 + 17,
        [Description("CCOM4_18")]
        CCom4Port18 = CCom4Port0 + 18,
        [Description("CCOM4_19")]
        CCom4Port19 = CCom4Port0 + 19,
        [Description("CCOM4_20")]
        CCom4Port20 = CCom4Port0 + 20,
        [Description("CCOM4_21")]
        CCom4Port21 = CCom4Port0 + 21,
        [Description("CCOM4_22")]
        CCom4Port22 = CCom4Port0 + 22,
        [Description("CCOM4_23")]
        CCom4Port23 = CCom4Port0 + 23,
        [Description("CCOM4_24")]
        CCom4Port24 = CCom4Port0 + 24,
        [Description("CCOM4_25")]
        CCom4Port25 = CCom4Port0 + 25,
        [Description("CCOM4_26")]
        CCom4Port26 = CCom4Port0 + 26,
        [Description("CCOM4_27")]
        CCom4Port27 = CCom4Port0 + 27,
        [Description("CCOM4_28")]
        CCom4Port28 = CCom4Port0 + 28,
        [Description("CCOM4_29")]
        CCom4Port29 = CCom4Port0 + 29,
        [Description("CCOM4_30")]
        CCom4Port30 = CCom4Port0 + 30,
        [Description("CCOM4_31")]
        CCom4Port31 = CCom4Port0 + 31,
        [Description("CCOM5")]
        CCom5Port0 = 8864,
        [Description("CCOM5_1")]
        CCom5Port1 = CCom5Port0 + 1,
        [Description("CCOM5_2")]
        CCom5Port2 = CCom5Port0 + 2,
        [Description("CCOM5_3")]
        CCom5Port3 = CCom5Port0 + 3,
        [Description("CCOM5_4")]
        CCom5Port4 = CCom5Port0 + 4,
        [Description("CCOM5_5")]
        CCom5Port5 = CCom5Port0 + 5,
        [Description("CCOM5_6")]
        CCom5Port6 = CCom5Port0 + 6,
        [Description("CCOM5_7")]
        CCom5Port7 = CCom5Port0 + 7,
        [Description("CCOM5_8")]
        CCom5Port8 = CCom5Port0 + 8,
        [Description("CCOM5_9")]
        CCom5Port9 = CCom5Port0 + 9,
        [Description("CCOM5_10")]
        CCom5Port10 = CCom5Port0 + 10,
        [Description("CCOM5_11")]
        CCom5Port11 = CCom5Port0 + 11,
        [Description("CCOM5_12")]
        CCom5Port12 = CCom5Port0 + 12,
        [Description("CCOM5_13")]
        CCom5Port13 = CCom5Port0 + 13,
        [Description("CCOM5_14")]
        CCom5Port14 = CCom5Port0 + 14,
        [Description("CCOM5_15")]
        CCom5Port15 = CCom5Port0 + 15,
        [Description("CCOM5_16")]
        CCom5Port16 = CCom5Port0 + 16,
        [Description("CCOM5_17")]
        CCom5Port17 = CCom5Port0 + 17,
        [Description("CCOM5_18")]
        CCom5Port18 = CCom5Port0 + 18,
        [Description("CCOM5_19")]
        CCom5Port19 = CCom5Port0 + 19,
        [Description("CCOM5_20")]
        CCom5Port20 = CCom5Port0 + 20,
        [Description("CCOM5_21")]
        CCom5Port21 = CCom5Port0 + 21,
        [Description("CCOM5_22")]
        CCom5Port22 = CCom5Port0 + 22,
        [Description("CCOM5_23")]
        CCom5Port23 = CCom5Port0 + 23,
        [Description("CCOM5_24")]
        CCom5Port24 = CCom5Port0 + 24,
        [Description("CCOM5_25")]
        CCom5Port25 = CCom5Port0 + 25,
        [Description("CCOM5_26")]
        CCom5Port26 = CCom5Port0 + 26,
        [Description("CCOM5_27")]
        CCom5Port27 = CCom5Port0 + 27,
        [Description("CCOM5_28")]
        CCom5Port28 = CCom5Port0 + 28,
        [Description("CCOM5_29")]
        CCom5Port29 = CCom5Port0 + 29,
        [Description("CCOM5_30")]
        CCom5Port30 = CCom5Port0 + 30,
        [Description("CCOM5_31")]
        CCom5Port31 = CCom5Port0 + 31,
        [Description("CCOM6")]
        CCom6Port0 = 9120,
        [Description("CCOM6_1")]
        CCom6Port1 = CCom6Port0 + 1,
        [Description("CCOM6_2")]
        CCom6Port2 = CCom6Port0 + 2,
        [Description("CCOM6_3")]
        CCom6Port3 = CCom6Port0 + 3,
        [Description("CCOM6_4")]
        CCom6Port4 = CCom6Port0 + 4,
        [Description("CCOM6_5")]
        CCom6Port5 = CCom6Port0 + 5,
        [Description("CCOM6_6")]
        CCom6Port6 = CCom6Port0 + 6,
        [Description("CCOM6_7")]
        CCom6Port7 = CCom6Port0 + 7,
        [Description("CCOM6_8")]
        CCom6Port8 = CCom6Port0 + 8,
        [Description("CCOM6_9")]
        CCom6Port9 = CCom6Port0 + 9,
        [Description("CCOM6_10")]
        CCom6Port10 = CCom6Port0 + 10,
        [Description("CCOM6_11")]
        CCom6Port11 = CCom6Port0 + 11,
        [Description("CCOM6_12")]
        CCom6Port12 = CCom6Port0 + 12,
        [Description("CCOM6_13")]
        CCom6Port13 = CCom6Port0 + 13,
        [Description("CCOM6_14")]
        CCom6Port14 = CCom6Port0 + 14,
        [Description("CCOM6_15")]
        CCom6Port15 = CCom6Port0 + 15,
        [Description("CCOM6_16")]
        CCom6Port16 = CCom6Port0 + 16,
        [Description("CCOM6_17")]
        CCom6Port17 = CCom6Port0 + 17,
        [Description("CCOM6_18")]
        CCom6Port18 = CCom6Port0 + 18,
        [Description("CCOM6_19")]
        CCom6Port19 = CCom6Port0 + 19,
        [Description("CCOM6_20")]
        CCom6Port20 = CCom6Port0 + 20,
        [Description("CCOM6_21")]
        CCom6Port21 = CCom6Port0 + 21,
        [Description("CCOM6_22")]
        CCom6Port22 = CCom6Port0 + 22,
        [Description("CCOM6_23")]
        CCom6Port23 = CCom6Port0 + 23,
        [Description("CCOM6_24")]
        CCom6Port24 = CCom6Port0 + 24,
        [Description("CCOM6_25")]
        CCom6Port25 = CCom6Port0 + 25,
        [Description("CCOM6_26")]
        CCom6Port26 = CCom6Port0 + 26,
        [Description("CCOM6_27")]
        CCom6Port27 = CCom6Port0 + 27,
        [Description("CCOM6_28")]
        CCom6Port28 = CCom6Port0 + 28,
        [Description("CCOM6_29")]
        CCom6Port29 = CCom6Port0 + 29,
        [Description("CCOM6_30")]
        CCom6Port30 = CCom6Port0 + 30,
        [Description("CCOM6_31")]
        CCom6Port31 = CCom6Port0 + 31,
        [Description("ICOM5")]
        ICom5Port0 = 9888,
        [Description("ICOM5_1")]
        ICom5Port1 = ICom5Port0 + 1,
        [Description("ICOM5_2")]
        ICom5Port2 = ICom5Port0 + 2,
        [Description("ICOM5_3")]
        ICom5Port3 = ICom5Port0 + 3,
        [Description("ICOM5_4")]
        ICom5Port4 = ICom5Port0 + 4,
        [Description("ICOM5_5")]
        ICom5Port5 = ICom5Port0 + 5,
        [Description("ICOM5_6")]
        ICom5Port6 = ICom5Port0 + 6,
        [Description("ICOM5_7")]
        ICom5Port7 = ICom5Port0 + 7,
        [Description("ICOM5_8")]
        ICom5Port8 = ICom5Port0 + 8,
        [Description("ICOM5_9")]
        ICom5Port9 = ICom5Port0 + 9,
        [Description("ICOM5_10")]
        ICom5Port10 = ICom5Port0 + 10,
        [Description("ICOM5_11")]
        ICom5Port11 = ICom5Port0 + 11,
        [Description("ICOM5_12")]
        ICom5Port12 = ICom5Port0 + 12,
        [Description("ICOM5_13")]
        ICom5Port13 = ICom5Port0 + 13,
        [Description("ICOM5_14")]
        ICom5Port14 = ICom5Port0 + 14,
        [Description("ICOM5_15")]
        ICom5Port15 = ICom5Port0 + 15,
        [Description("ICOM5_16")]
        ICom5Port16 = ICom5Port0 + 16,
        [Description("ICOM5_17")]
        ICom5Port17 = ICom5Port0 + 17,
        [Description("ICOM5_18")]
        ICom5Port18 = ICom5Port0 + 18,
        [Description("ICOM5_19")]
        ICom5Port19 = ICom5Port0 + 19,
        [Description("ICOM5_20")]
        ICom5Port20 = ICom5Port0 + 20,
        [Description("ICOM5_21")]
        ICom5Port21 = ICom5Port0 + 21,
        [Description("ICOM5_22")]
        ICom5Port22 = ICom5Port0 + 22,
        [Description("ICOM5_23")]
        ICom5Port23 = ICom5Port0 + 23,
        [Description("ICOM5_24")]
        ICom5Port24 = ICom5Port0 + 24,
        [Description("ICOM5_25")]
        ICom5Port25 = ICom5Port0 + 25,
        [Description("ICOM5_26")]
        ICom5Port26 = ICom5Port0 + 26,
        [Description("ICOM5_27")]
        ICom5Port27 = ICom5Port0 + 27,
        [Description("ICOM5_28")]
        ICom5Port28 = ICom5Port0 + 28,
        [Description("ICOM5_29")]
        ICom5Port29 = ICom5Port0 + 29,
        [Description("ICOM5_30")]
        ICom5Port30 = ICom5Port0 + 30,
        [Description("ICOM5_31")]
        ICom5Port31 = ICom5Port0 + 31,
        [Description("ICOM6")]
        ICom6Port0 = 10144,
        [Description("ICOM6_1")]
        ICom6Port1 = ICom6Port0 + 1,
        [Description("ICOM6_2")]
        ICom6Port2 = ICom6Port0 + 2,
        [Description("ICOM6_3")]
        ICom6Port3 = ICom6Port0 + 3,
        [Description("ICOM6_4")]
        ICom6Port4 = ICom6Port0 + 4,
        [Description("ICOM6_5")]
        ICom6Port5 = ICom6Port0 + 5,
        [Description("ICOM6_6")]
        ICom6Port6 = ICom6Port0 + 6,
        [Description("ICOM6_7")]
        ICom6Port7 = ICom6Port0 + 7,
        [Description("ICOM6_8")]
        ICom6Port8 = ICom6Port0 + 8,
        [Description("ICOM6_9")]
        ICom6Port9 = ICom6Port0 + 9,
        [Description("ICOM6_10")]
        ICom6Port10 = ICom6Port0 + 10,
        [Description("ICOM6_11")]
        ICom6Port11 = ICom6Port0 + 11,
        [Description("ICOM6_12")]
        ICom6Port12 = ICom6Port0 + 12,
        [Description("ICOM6_13")]
        ICom6Port13 = ICom6Port0 + 13,
        [Description("ICOM6_14")]
        ICom6Port14 = ICom6Port0 + 14,
        [Description("ICOM6_15")]
        ICom6Port15 = ICom6Port0 + 15,
        [Description("ICOM6_16")]
        ICom6Port16 = ICom6Port0 + 16,
        [Description("ICOM6_17")]
        ICom6Port17 = ICom6Port0 + 17,
        [Description("ICOM6_18")]
        ICom6Port18 = ICom6Port0 + 18,
        [Description("ICOM6_19")]
        ICom6Port19 = ICom6Port0 + 19,
        [Description("ICOM6_20")]
        ICom6Port20 = ICom6Port0 + 20,
        [Description("ICOM6_21")]
        ICom6Port21 = ICom6Port0 + 21,
        [Description("ICOM6_22")]
        ICom6Port22 = ICom6Port0 + 22,
        [Description("ICOM6_23")]
        ICom6Port23 = ICom6Port0 + 23,
        [Description("ICOM6_24")]
        ICom6Port24 = ICom6Port0 + 24,
        [Description("ICOM6_25")]
        ICom6Port25 = ICom6Port0 + 25,
        [Description("ICOM6_26")]
        ICom6Port26 = ICom6Port0 + 26,
        [Description("ICOM6_27")]
        ICom6Port27 = ICom6Port0 + 27,
        [Description("ICOM6_28")]
        ICom6Port28 = ICom6Port0 + 28,
        [Description("ICOM6_29")]
        ICom6Port29 = ICom6Port0 + 29,
        [Description("ICOM6_30")]
        ICom6Port30 = ICom6Port0 + 30,
        [Description("ICOM6_31")]
        ICom6Port31 = ICom6Port0 + 31,
        [Description("ICOM7")]
        ICom7Port0 = 10400,
        [Description("ICOM7_1")]
        ICom7Port1 = ICom7Port0 + 1,
        [Description("ICOM7_2")]
        ICom7Port2 = ICom7Port0 + 2,
        [Description("ICOM7_3")]
        ICom7Port3 = ICom7Port0 + 3,
        [Description("ICOM7_4")]
        ICom7Port4 = ICom7Port0 + 4,
        [Description("ICOM7_5")]
        ICom7Port5 = ICom7Port0 + 5,
        [Description("ICOM7_6")]
        ICom7Port6 = ICom7Port0 + 6,
        [Description("ICOM7_7")]
        ICom7Port7 = ICom7Port0 + 7,
        [Description("ICOM7_8")]
        ICom7Port8 = ICom7Port0 + 8,
        [Description("ICOM7_9")]
        ICom7Port9 = ICom7Port0 + 9,
        [Description("ICOM7_10")]
        ICom7Port10 = ICom7Port0 + 10,
        [Description("ICOM7_11")]
        ICom7Port11 = ICom7Port0 + 11,
        [Description("ICOM7_12")]
        ICom7Port12 = ICom7Port0 + 12,
        [Description("ICOM7_13")]
        ICom7Port13 = ICom7Port0 + 13,
        [Description("ICOM7_14")]
        ICom7Port14 = ICom7Port0 + 14,
        [Description("ICOM7_15")]
        ICom7Port15 = ICom7Port0 + 15,
        [Description("ICOM7_16")]
        ICom7Port16 = ICom7Port0 + 16,
        [Description("ICOM7_17")]
        ICom7Port17 = ICom7Port0 + 17,
        [Description("ICOM7_18")]
        ICom7Port18 = ICom7Port0 + 18,
        [Description("ICOM7_19")]
        ICom7Port19 = ICom7Port0 + 19,
        [Description("ICOM7_20")]
        ICom7Port20 = ICom7Port0 + 20,
        [Description("ICOM7_21")]
        ICom7Port21 = ICom7Port0 + 21,
        [Description("ICOM7_22")]
        ICom7Port22 = ICom7Port0 + 22,
        [Description("ICOM7_23")]
        ICom7Port23 = ICom7Port0 + 23,
        [Description("ICOM7_24")]
        ICom7Port24 = ICom7Port0 + 24,
        [Description("ICOM7_25")]
        ICom7Port25 = ICom7Port0 + 25,
        [Description("ICOM7_26")]
        ICom7Port26 = ICom7Port0 + 26,
        [Description("ICOM7_27")]
        ICom7Port27 = ICom7Port0 + 27,
        [Description("ICOM7_28")]
        ICom7Port28 = ICom7Port0 + 28,
        [Description("ICOM7_29")]
        ICom7Port29 = ICom7Port0 + 29,
        [Description("ICOM7_30")]
        ICom7Port30 = ICom7Port0 + 30,
        [Description("ICOM7_31")]
        ICom7Port31 = ICom7Port0 + 31,
        [Description("SCOM1")]
        SCom1Port0 = 10656,
        [Description("SCOM1_1")]
        SCom1Port1 = SCom1Port0 + 1,
        [Description("SCOM1_2")]
        SCom1Port2 = SCom1Port0 + 2,
        [Description("SCOM1_3")]
        SCom1Port3 = SCom1Port0 + 3,
        [Description("SCOM1_4")]
        SCom1Port4 = SCom1Port0 + 4,
        [Description("SCOM1_5")]
        SCom1Port5 = SCom1Port0 + 5,
        [Description("SCOM1_6")]
        SCom1Port6 = SCom1Port0 + 6,
        [Description("SCOM1_7")]
        SCom1Port7 = SCom1Port0 + 7,
        [Description("SCOM1_8")]
        SCom1Port8 = SCom1Port0 + 8,
        [Description("SCOM1_9")]
        SCom1Port9 = SCom1Port0 + 9,
        [Description("SCOM1_10")]
        SCom1Port10 = SCom1Port0 + 10,
        [Description("SCOM1_11")]
        SCom1Port11 = SCom1Port0 + 11,
        [Description("SCOM1_12")]
        SCom1Port12 = SCom1Port0 + 12,
        [Description("SCOM1_13")]
        SCom1Port13 = SCom1Port0 + 13,
        [Description("SCOM1_14")]
        SCom1Port14 = SCom1Port0 + 14,
        [Description("SCOM1_15")]
        SCom1Port15 = SCom1Port0 + 15,
        [Description("SCOM1_16")]
        SCom1Port16 = SCom1Port0 + 16,
        [Description("SCOM1_17")]
        SCom1Port17 = SCom1Port0 + 17,
        [Description("SCOM1_18")]
        SCom1Port18 = SCom1Port0 + 18,
        [Description("SCOM1_19")]
        SCom1Port19 = SCom1Port0 + 19,
        [Description("SCOM1_20")]
        SCom1Port20 = SCom1Port0 + 20,
        [Description("SCOM1_21")]
        SCom1Port21 = SCom1Port0 + 21,
        [Description("SCOM1_22")]
        SCom1Port22 = SCom1Port0 + 22,
        [Description("SCOM1_23")]
        SCom1Port23 = SCom1Port0 + 23,
        [Description("SCOM1_24")]
        SCom1Port24 = SCom1Port0 + 24,
        [Description("SCOM1_25")]
        SCom1Port25 = SCom1Port0 + 25,
        [Description("SCOM1_26")]
        SCom1Port26 = SCom1Port0 + 26,
        [Description("SCOM1_27")]
        SCom1Port27 = SCom1Port0 + 27,
        [Description("SCOM1_28")]
        SCom1Port28 = SCom1Port0 + 28,
        [Description("SCOM1_29")]
        SCom1Port29 = SCom1Port0 + 29,
        [Description("SCOM1_30")]
        SCom1Port30 = SCom1Port0 + 30,
        [Description("SCOM1_31")]
        SCom1Port31 = SCom1Port0 + 31,
        [Description("SCOM2")]
        SCom2Port0 = 10912,
        [Description("SCOM2_1")]
        SCom2Port1 = SCom2Port0 + 1,
        [Description("SCOM2_2")]
        SCom2Port2 = SCom2Port0 + 2,
        [Description("SCOM2_3")]
        SCom2Port3 = SCom2Port0 + 3,
        [Description("SCOM2_4")]
        SCom2Port4 = SCom2Port0 + 4,
        [Description("SCOM2_5")]
        SCom2Port5 = SCom2Port0 + 5,
        [Description("SCOM2_6")]
        SCom2Port6 = SCom2Port0 + 6,
        [Description("SCOM2_7")]
        SCom2Port7 = SCom2Port0 + 7,
        [Description("SCOM2_8")]
        SCom2Port8 = SCom2Port0 + 8,
        [Description("SCOM2_9")]
        SCom2Port9 = SCom2Port0 + 9,
        [Description("SCOM2_10")]
        SCom2Port10 = SCom2Port0 + 10,
        [Description("SCOM2_11")]
        SCom2Port11 = SCom2Port0 + 11,
        [Description("SCOM2_12")]
        SCom2Port12 = SCom2Port0 + 12,
        [Description("SCOM2_13")]
        SCom2Port13 = SCom2Port0 + 13,
        [Description("SCOM2_14")]
        SCom2Port14 = SCom2Port0 + 14,
        [Description("SCOM2_15")]
        SCom2Port15 = SCom2Port0 + 15,
        [Description("SCOM2_16")]
        SCom2Port16 = SCom2Port0 + 16,
        [Description("SCOM2_17")]
        SCom2Port17 = SCom2Port0 + 17,
        [Description("SCOM2_18")]
        SCom2Port18 = SCom2Port0 + 18,
        [Description("SCOM2_19")]
        SCom2Port19 = SCom2Port0 + 19,
        [Description("SCOM2_20")]
        SCom2Port20 = SCom2Port0 + 20,
        [Description("SCOM2_21")]
        SCom2Port21 = SCom2Port0 + 21,
        [Description("SCOM2_22")]
        SCom2Port22 = SCom2Port0 + 22,
        [Description("SCOM2_23")]
        SCom2Port23 = SCom2Port0 + 23,
        [Description("SCOM2_24")]
        SCom2Port24 = SCom2Port0 + 24,
        [Description("SCOM2_25")]
        SCom2Port25 = SCom2Port0 + 25,
        [Description("SCOM2_26")]
        SCom2Port26 = SCom2Port0 + 26,
        [Description("SCOM2_27")]
        SCom2Port27 = SCom2Port0 + 27,
        [Description("SCOM2_28")]
        SCom2Port28 = SCom2Port0 + 28,
        [Description("SCOM2_29")]
        SCom2Port29 = SCom2Port0 + 29,
        [Description("SCOM2_30")]
        SCom2Port30 = SCom2Port0 + 30,
        [Description("SCOM2_31")]
        SCom2Port31 = SCom2Port0 + 31,
        [Description("SCOM3")]
        SCom3Port0 = 11168,
        [Description("SCOM3_1")]
        SCom3Port1 = SCom3Port0 + 1,
        [Description("SCOM3_2")]
        SCom3Port2 = SCom3Port0 + 2,
        [Description("SCOM3_3")]
        SCom3Port3 = SCom3Port0 + 3,
        [Description("SCOM3_4")]
        SCom3Port4 = SCom3Port0 + 4,
        [Description("SCOM3_5")]
        SCom3Port5 = SCom3Port0 + 5,
        [Description("SCOM3_6")]
        SCom3Port6 = SCom3Port0 + 6,
        [Description("SCOM3_7")]
        SCom3Port7 = SCom3Port0 + 7,
        [Description("SCOM3_8")]
        SCom3Port8 = SCom3Port0 + 8,
        [Description("SCOM3_9")]
        SCom3Port9 = SCom3Port0 + 9,
        [Description("SCOM3_10")]
        SCom3Port10 = SCom3Port0 + 10,
        [Description("SCOM3_11")]
        SCom3Port11 = SCom3Port0 + 11,
        [Description("SCOM3_12")]
        SCom3Port12 = SCom3Port0 + 12,
        [Description("SCOM3_13")]
        SCom3Port13 = SCom3Port0 + 13,
        [Description("SCOM3_14")]
        SCom3Port14 = SCom3Port0 + 14,
        [Description("SCOM3_15")]
        SCom3Port15 = SCom3Port0 + 15,
        [Description("SCOM3_16")]
        SCom3Port16 = SCom3Port0 + 16,
        [Description("SCOM3_17")]
        SCom3Port17 = SCom3Port0 + 17,
        [Description("SCOM3_18")]
        SCom3Port18 = SCom3Port0 + 18,
        [Description("SCOM3_19")]
        SCom3Port19 = SCom3Port0 + 19,
        [Description("SCOM3_20")]
        SCom3Port20 = SCom3Port0 + 20,
        [Description("SCOM3_21")]
        SCom3Port21 = SCom3Port0 + 21,
        [Description("SCOM3_22")]
        SCom3Port22 = SCom3Port0 + 22,
        [Description("SCOM3_23")]
        SCom3Port23 = SCom3Port0 + 23,
        [Description("SCOM3_24")]
        SCom3Port24 = SCom3Port0 + 24,
        [Description("SCOM3_25")]
        SCom3Port25 = SCom3Port0 + 25,
        [Description("SCOM3_26")]
        SCom3Port26 = SCom3Port0 + 26,
        [Description("SCOM3_27")]
        SCom3Port27 = SCom3Port0 + 27,
        [Description("SCOM3_28")]
        SCom3Port28 = SCom3Port0 + 28,
        [Description("SCOM3_29")]
        SCom3Port29 = SCom3Port0 + 29,
        [Description("SCOM3_30")]
        SCom3Port30 = SCom3Port0 + 30,
        [Description("SCOM3_31")]
        SCom3Port31 = SCom3Port0 + 31,
        [Description("SCOM4")]
        SCom4Port0 = 11424,
        [Description("SCOM4_1")]
        SCom4Port1 = SCom4Port0 + 1,
        [Description("SCOM4_2")]
        SCom4Port2 = SCom4Port0 + 2,
        [Description("SCOM4_3")]
        SCom4Port3 = SCom4Port0 + 3,
        [Description("SCOM4_4")]
        SCom4Port4 = SCom4Port0 + 4,
        [Description("SCOM4_5")]
        SCom4Port5 = SCom4Port0 + 5,
        [Description("SCOM4_6")]
        SCom4Port6 = SCom4Port0 + 6,
        [Description("SCOM4_7")]
        SCom4Port7 = SCom4Port0 + 7,
        [Description("SCOM4_8")]
        SCom4Port8 = SCom4Port0 + 8,
        [Description("SCOM4_9")]
        SCom4Port9 = SCom4Port0 + 9,
        [Description("SCOM4_10")]
        SCom4Port10 = SCom4Port0 + 10,
        [Description("SCOM4_11")]
        SCom4Port11 = SCom4Port0 + 11,
        [Description("SCOM4_12")]
        SCom4Port12 = SCom4Port0 + 12,
        [Description("SCOM4_13")]
        SCom4Port13 = SCom4Port0 + 13,
        [Description("SCOM4_14")]
        SCom4Port14 = SCom4Port0 + 14,
        [Description("SCOM4_15")]
        SCom4Port15 = SCom4Port0 + 15,
        [Description("SCOM4_16")]
        SCom4Port16 = SCom4Port0 + 16,
        [Description("SCOM4_17")]
        SCom4Port17 = SCom4Port0 + 17,
        [Description("SCOM4_18")]
        SCom4Port18 = SCom4Port0 + 18,
        [Description("SCOM4_19")]
        SCom4Port19 = SCom4Port0 + 19,
        [Description("SCOM4_20")]
        SCom4Port20 = SCom4Port0 + 20,
        [Description("SCOM4_21")]
        SCom4Port21 = SCom4Port0 + 21,
        [Description("SCOM4_22")]
        SCom4Port22 = SCom4Port0 + 22,
        [Description("SCOM4_23")]
        SCom4Port23 = SCom4Port0 + 23,
        [Description("SCOM4_24")]
        SCom4Port24 = SCom4Port0 + 24,
        [Description("SCOM4_25")]
        SCom4Port25 = SCom4Port0 + 25,
        [Description("SCOM4_26")]
        SCom4Port26 = SCom4Port0 + 26,
        [Description("SCOM4_27")]
        SCom4Port27 = SCom4Port0 + 27,
        [Description("SCOM4_28")]
        SCom4Port28 = SCom4Port0 + 28,
        [Description("SCOM4_29")]
        SCom4Port29 = SCom4Port0 + 29,
        [Description("SCOM4_30")]
        SCom4Port30 = SCom4Port0 + 30,
        [Description("SCOM4_31")]
        SCom4Port31 = SCom4Port0 + 31,
    }
}
