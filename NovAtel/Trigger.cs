﻿using System.ComponentModel;

namespace NovAtel
{
    enum Trigger
    {
        [Description("ONNEW")]
        OnNew = 0,
        [Description("ONCHANGED")]
        OnChanged = 1,
        [Description("ONTIME")]
        OnTime = 2,
        [Description("ONNEXT")]
        OnNext = 3,
        [Description("ONCE")]
        Once = 4,
        [Description("ONMARK")]
        OnMark = 5,
    }
}
