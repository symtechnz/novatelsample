﻿namespace NovAtel.MessageHandlers
{
    public interface IMessageHandler
    {
        public int MessageId { get; }
        public string MessageName { get; }
        public void Handle(Message message);
    }
}
