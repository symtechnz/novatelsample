﻿using System;
using System.Buffers.Binary;
using System.Text;

namespace NovAtel.MessageHandlers
{
    class BestPosMessageHandler : IMessageHandler
    {
        public int MessageId => 42;

        public string MessageName => "BESTPOS";

        public void Handle(Message message)
        {
            var content = message.MessageContent.Span;
            Console.WriteLine("--- Position received ---");
            var solution = (SolutionStatus)BinaryPrimitives.ReadUInt32LittleEndian(content.Slice(0, 4));
            Console.WriteLine(solution.GetDescription());
            var type = (PositionType)BinaryPrimitives.ReadUInt32LittleEndian(content.Slice(4, 4));
            Console.WriteLine("Position type    : {0}", type.GetDescription());
            var latitude = BinaryPrimitives.ReadDoubleLittleEndian(content.Slice(8, 8));
            var longitude = BinaryPrimitives.ReadDoubleLittleEndian(content.Slice(16, 8));
            var altitude = BinaryPrimitives.ReadDoubleLittleEndian(content.Slice(24, 8));
            Console.WriteLine("Position         : {0,10:##0.000000} {1,11:###0.000000} {2,6:###0.0}", latitude, longitude, altitude);
            var undulation = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(32, 4));
            var datumId = BinaryPrimitives.ReadInt32LittleEndian(content.Slice(36, 4));
            var latitudeDeviation = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(40, 4));
            var longitudeDeviation = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(44, 4));
            var altitudeDeviation = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(48, 4));
            Console.WriteLine("Deviation        : {0,7:##0.000}     {1,7:##0.000}      {2,6:##0.000}", latitudeDeviation, longitudeDeviation, altitudeDeviation);
            Console.WriteLine("Undulation       : {0}", undulation);
            Console.WriteLine("Datum            : {0}", datumId == 61 ? "WGS84" : datumId == 63 ? "USER" : "UNKNOWN");

            var stationId = content.Slice(52, 4).AsAsciiString();
            if (!string.IsNullOrWhiteSpace(stationId))
                Console.WriteLine("Station ID       : {0}", stationId);

            var diffAge = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(56, 4));
            Console.WriteLine("Differential age : {0}", diffAge);
            var solutionAge = BinaryPrimitives.ReadSingleLittleEndian(content.Slice(60, 4));
            Console.WriteLine("Solution age     : {0}", solutionAge);
            var satellitesSeen = content[64];
            Console.WriteLine("Satellites seen  : {0}", satellitesSeen);
            var satellitesUsed = content[65];
            Console.WriteLine("Satellites used  : {0}", satellitesUsed);
        }
    }
}
