﻿using System;

namespace NovAtel.MessageHandlers
{
    class LogMessageHandler : IMessageHandler
    {
        public int MessageId => 1;

        public string MessageName => "LOG";

        public void Handle(Message message)
        {
            throw new InvalidOperationException("LOG messages should only ever go from the client to the receiver");
        }
    }
}
