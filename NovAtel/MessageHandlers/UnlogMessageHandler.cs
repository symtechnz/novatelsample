﻿using System;

namespace NovAtel.MessageHandlers
{
    class UnlogMessageHandler : IMessageHandler
    {
        public int MessageId => 36;

        public string MessageName => "UNLOG";

        public void Handle(Message message)
        {
            throw new InvalidOperationException("UNLOG messages should only ever go from the client to the receiver");
        }
    }
}
