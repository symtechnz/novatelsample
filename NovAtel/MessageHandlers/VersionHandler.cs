﻿using System;
using System.Buffers.Binary;
using System.ComponentModel;

namespace NovAtel.MessageHandlers
{
    class VersionHandler : IMessageHandler
    {
        public int MessageId => 37;

        public string MessageName => "VERSION";

        public void Handle(Message message)
        {
            var componentCount = BinaryPrimitives.ReadUInt32LittleEndian(message.MessageContent.Span);
            Console.WriteLine("--- Version received ---");
            for (int i = 0; i < componentCount; i++)
            {
                var componentData = message.MessageContent.Slice(4 + i * 108, 108).Span;
                var type = (ComponentType)BinaryPrimitives.ReadUInt32LittleEndian(componentData.Slice(0, 4));
                Console.WriteLine(type.GetDescription());

                var model = componentData.Slice(4, 16).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(model))
                    Console.WriteLine("    Model             : {0}", model);

                var psn = componentData.Slice(20, 16).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(psn))
                    Console.WriteLine("    Serial number     : {0}", psn);

                var hwVersion = componentData.Slice(36, 16).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(hwVersion))
                    Console.WriteLine("    Hardware version  : {0}", hwVersion);

                var swVersion = componentData.Slice(52, 16).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(swVersion))
                    Console.WriteLine("    Software version  : {0}", swVersion);

                var bootVersion = componentData.Slice(68, 16).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(bootVersion))
                    Console.WriteLine("    Bootload version  : {0}", bootVersion);

                var compDate = componentData.Slice(84, 12).AsAsciiString();
                var compTime = componentData.Slice(96, 12).AsAsciiString();
                if (!string.IsNullOrWhiteSpace(compDate) || !string.IsNullOrWhiteSpace(compTime))
                    Console.WriteLine("    Compiled at       : {0} {1}", compDate, compTime);
            }
        }

        enum ComponentType
        {
            [Description("Unknown component")]
            Unknown = 0,
            [Description("OEM7 family receiver")]
            GpsCard = 1,
            Controller = 2,
            [Description("OEM card enclosure")]
            Enclosure = 3,
            [Description("IMU integrated in the enclosure")]
            ImuCard = 7,
            [Description("Application specific information")]
            UserInfo = 8,
            [Description("Wi-Fi radio firmware")]
            WiFi = 15,
            [Description("UHF radio component")]
            Radio = 18,
            [Description("Web server content")]
            WwwContent = 19,
            [Description("Regulatory configuration")]
            Regulatory = 20,
            [Description("OEM7 FPGA version")]
            Oem7Fpga = 21,
            [Description("Embedded application")]
            Application = 22,
            [Description("Package")]
            Package = 23,
            [Description("Default configuration data")]
            DefaultConfig = 25,
            [Description("Wheel sensor in the enclosure")]
            WheelSensor = 26,
            [Description("Embedded Auth Code data")]
            EmbeddedAuth = 27,
            [Description("Height/track model data")]
            DbHeightModel = 981073920,
            [Description("Web UI ISO Image")]
            DbWwwIso = 981073928,
            [Description("Lua Script ISO Image")]
            DbLuaScripts = 981073930,
        }
    }
}
