﻿using System;

namespace NovAtel
{
    public class Message
    {
        public ushort MessageId { get; set; }
        public MessageType MessageType { get; set; }
        public Port PortAddress { get; set; }
        public ushort MessageLength { get; set; }
        public ushort Sequence { get; set; }
        public byte IdleTime { get; set; }
        public byte TimeStatus { get; set; }
        public ushort Week { get; set; }
        public uint Milliseconds { get; set; }
        public uint ReceiverStatus { get; set; }
        public ushort Reserved { get; set; }
        public ushort ReceiverSoftwareVersion { get; set; }
        public ReadOnlyMemory<byte> MessageContent { get; set; }
    }
}
