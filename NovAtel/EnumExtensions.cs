﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace NovAtel
{
    public static class EnumExtensions
    {
        public static string GetDescription<TEnum>(this TEnum value) where TEnum : Enum
        {
            var name = value.ToString();
            var field = typeof(TEnum).GetField(name, BindingFlags.Public | BindingFlags.Static);
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length == 0)
                return name;

            return ((DescriptionAttribute)attributes[0]).Description;
        }
    }
}
