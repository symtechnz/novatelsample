﻿namespace NovAtel
{
    public enum MessageFormat
    {
        Binary = 0,
        ASCII = 1,
        AbbreviatedASCII,
    }
}
